<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-387,2119,898,0.826447,35,166>
  <Grid=10,10,1>
  <DataSet=acq_bessel6th.dat>
  <DataDisplay=acq_bessel6th.dpl>
  <OpenDisplay=1>
  <Script=acq_bessel6th.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 220 130 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 220 160 0 0 0 0>
  <L L1 1 370 130 8 -26 0 1 "2.672uH" 1 "" 0>
  <C C1 1 340 130 -8 46 0 1 "1.975pF" 1 "" 0 "neutral" 0>
  <GND * 1 370 160 0 0 0 0>
  <L L2 1 480 50 -26 -44 0 0 "14.48nH" 1 "" 0>
  <C C2 1 420 50 -26 10 0 0 "364.6pF" 1 "" 0 "neutral" 0>
  <L L3 1 510 130 8 -26 0 1 "570.6nH" 1 "" 0>
  <C C3 1 480 130 -8 46 0 1 "9.248pF" 1 "" 0 "neutral" 0>
  <GND * 1 510 160 0 0 0 0>
  <L L4 1 620 50 -26 -44 0 0 "30.88nH" 1 "" 0>
  <C C4 1 560 50 -26 10 0 0 "170.9pF" 1 "" 0 "neutral" 0>
  <L L5 1 650 130 8 -26 0 1 "327.8nH" 1 "" 0>
  <C C5 1 620 130 -8 46 0 1 "16.1pF" 1 "" 0 "neutral" 0>
  <GND * 1 650 160 0 0 0 0>
  <L L6 1 760 50 -26 -44 0 0 "81.91nH" 1 "" 0>
  <C C6 1 700 50 -26 10 0 0 "64.43pF" 1 "" 0 "neutral" 0>
  <Pac P2 1 790 130 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 790 160 0 0 0 0>
  <.SP SP1 1 940 -110 0 67 0 0 "log" 1 "1MHz" 1 "0.5GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1140 -100 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
</Components>
<Wires>
  <220 50 220 100 "" 0 0 0 "">
  <220 50 370 50 "" 0 0 0 "">
  <370 50 370 100 "" 0 0 0 "">
  <510 50 510 100 "" 0 0 0 "">
  <650 50 650 100 "" 0 0 0 "">
  <790 50 790 100 "" 0 0 0 "">
  <370 50 390 50 "" 0 0 0 "">
  <340 100 370 100 "" 0 0 0 "">
  <340 160 370 160 "" 0 0 0 "">
  <510 50 530 50 "" 0 0 0 "">
  <480 100 510 100 "" 0 0 0 "">
  <480 160 510 160 "" 0 0 0 "">
  <650 50 670 50 "" 0 0 0 "">
  <620 100 650 100 "" 0 0 0 "">
  <620 160 650 160 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 200 575 620 344 3 #c0c0c0 1 00 1 0 0.2 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 70 -120 850 890 #000000 0 1 #c0c0c0 1 0>
  <Text 360 -100 16 #000000 0 "Bessel band-pass filter \n 20MHz...240MHz, pi-type, \n impedance matching 50 Ohm">
  <Text 170 630 12 #aa0000 0 "Two Reasons Why This won't work\n===========================\n1. This doesn't even look like a BPF. \n2. I would like the inductors to be lower in value. \n===========================\nSame goes for its tee type alternative.\n">
</Paintings>
