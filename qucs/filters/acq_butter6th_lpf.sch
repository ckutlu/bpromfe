<Qucs Schematic 0.0.19>
<Properties>
  <View=0,0,1633,930,1,48,0>
  <Grid=10,10,1>
  <DataSet=acq_butter6th_lpf.dat>
  <DataDisplay=acq_butter6th_lpf.dpl>
  <OpenDisplay=1>
  <Script=acq_butter6th_lpf.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 900 30 0 67 0 0 "log" 1 "1MHz" 1 "0.5GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1100 40 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <Pac P1 1 190 250 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 190 280 0 0 0 0>
  <C C1 1 300 250 17 -26 0 1 "6.865pF" 1 "" 0 "neutral" 0>
  <GND * 1 300 280 0 0 0 0>
  <L L1 1 370 170 -26 10 0 0 "46.89nH" 1 "" 0>
  <C C2 1 440 250 17 -26 0 1 "25.62pF" 1 "" 0 "neutral" 0>
  <GND * 1 440 280 0 0 0 0>
  <L L2 1 510 170 -26 10 0 0 "64.05nH" 1 "" 0>
  <C C3 1 580 250 17 -26 0 1 "18.76pF" 1 "" 0 "neutral" 0>
  <GND * 1 580 280 0 0 0 0>
  <L L3 1 650 170 -26 10 0 0 "17.16nH" 1 "" 0>
  <Pac P2 1 720 250 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 720 280 0 0 0 0>
</Components>
<Wires>
  <190 170 190 220 "" 0 0 0 "">
  <190 170 300 170 "" 0 0 0 "">
  <300 170 300 220 "" 0 0 0 "">
  <440 170 440 220 "" 0 0 0 "">
  <580 170 580 220 "" 0 0 0 "">
  <720 170 720 220 "" 0 0 0 "">
  <300 170 340 170 "" 0 0 0 "">
  <400 170 440 170 "" 0 0 0 "">
  <440 170 480 170 "" 0 0 0 "">
  <540 170 580 170 "" 0 0 0 "">
  <580 170 620 170 "" 0 0 0 "">
  <680 170 720 170 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 160 715 620 344 3 #c0c0c0 1 00 1 0 5e+07 5e+08 1 -120 20 10.7099 1 -1 0.5 1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 30 20 850 890 #000000 0 1 #c0c0c0 1 0>
  <Text 320 40 16 #000000 0 "Butterworth low-pass filter \n 240MHz cutoff, pi-type, \n impedance matching 50 Ohm">
</Paintings>
