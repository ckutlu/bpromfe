<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-349,2119,845,0.826447,35,76>
  <Grid=10,10,1>
  <DataSet=acq_cheby5th_bpf.dat>
  <DataDisplay=acq_cheby5th_bpf.dpl>
  <OpenDisplay=1>
  <Script=acq_cheby5th_bpf.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 940 -110 0 67 0 0 "log" 1 "1MHz" 1 "0.5GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1140 -100 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <Pac P1 1 220 110 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 220 140 0 0 0 0>
  <L L1 1 370 110 8 -26 0 1 "170.8nH" 1 "" 0>
  <C C1 1 340 110 -8 46 0 1 "30.89pF" 1 "" 0 "neutral" 0>
  <GND * 1 370 140 0 0 0 0>
  <L L2 1 480 30 -26 -44 0 0 "39.47nH" 1 "" 0>
  <C C2 1 420 30 -26 10 0 0 "133.7pF" 1 "" 0 "neutral" 0>
  <L L3 1 510 110 8 -26 0 1 "121.5nH" 1 "" 0>
  <C C3 1 480 110 -8 46 0 1 "43.42pF" 1 "" 0 "neutral" 0>
  <GND * 1 510 140 0 0 0 0>
  <L L4 1 620 30 -26 -44 0 0 "39.47nH" 1 "" 0>
  <C C4 1 560 30 -26 10 0 0 "133.7pF" 1 "" 0 "neutral" 0>
  <L L5 1 650 110 8 -26 0 1 "170.8nH" 1 "" 0>
  <C C5 1 620 110 -8 46 0 1 "30.89pF" 1 "" 0 "neutral" 0>
  <GND * 1 650 140 0 0 0 0>
  <Pac P2 1 760 110 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 760 140 0 0 0 0>
</Components>
<Wires>
  <220 30 220 80 "" 0 0 0 "">
  <220 30 370 30 "" 0 0 0 "">
  <370 30 370 80 "" 0 0 0 "">
  <510 30 510 80 "" 0 0 0 "">
  <650 30 650 80 "" 0 0 0 "">
  <370 30 390 30 "" 0 0 0 "">
  <340 80 370 80 "" 0 0 0 "">
  <340 140 370 140 "" 0 0 0 "">
  <510 30 530 30 "" 0 0 0 "">
  <480 80 510 80 "" 0 0 0 "">
  <480 140 510 140 "" 0 0 0 "">
  <620 80 650 80 "" 0 0 0 "">
  <620 140 650 140 "" 0 0 0 "">
  <760 30 760 80 "" 0 0 0 "">
  <650 30 760 30 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 200 575 620 344 3 #c0c0c0 1 00 1 0 5e+07 5e+08 1 -120 20 10.7099 1 -1 0.5 1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 70 -120 850 890 #000000 0 1 #c0c0c0 1 0>
  <Text 360 -100 16 #000000 0 "Chebyshev band-pass filter \n 20MHz...240MHz, pi-type, \n impedance matching 50 Ohm">
</Paintings>
