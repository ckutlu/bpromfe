<Qucs Schematic 0.0.19>
<Properties>
  <View=0,0,899,800,1,0,0>
  <Grid=10,10,1>
  <DataSet=lpf_butter_400M.dat>
  <DataDisplay=lpf_butter_400M.dpl>
  <OpenDisplay=1>
  <Script=lpf_butter_400M.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 310 310 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 310 340 0 0 0 0>
  <L L1 1 420 230 -26 10 0 0 "19.89nH" 1 "" 0>
  <C C1 1 490 310 17 -26 0 1 "15.92pF" 1 "" 0 "neutral" 0>
  <GND * 1 490 340 0 0 0 0>
  <L L2 1 560 230 -26 10 0 0 "19.89nH" 1 "" 0>
  <Pac P2 1 630 310 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 630 340 0 0 0 0>
  <.SP SP1 1 320 410 0 67 0 0 "log" 1 "40MHz" 1 "4GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 540 420 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
</Components>
<Wires>
  <310 230 310 280 "" 0 0 0 "">
  <310 230 390 230 "" 0 0 0 "">
  <490 230 490 280 "" 0 0 0 "">
  <630 230 630 280 "" 0 0 0 "">
  <490 230 530 230 "" 0 0 0 "">
  <450 230 490 230 "" 0 0 0 "">
  <590 230 630 230 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Text 650 410 12 #000000 0 "Butterworth low-pass filter \n 400MHz cutoff, tee-type, \n impedance matching 50 Ohm">
</Paintings>
