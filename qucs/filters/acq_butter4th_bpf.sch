<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-387,2119,898,0.826447,35,166>
  <Grid=10,10,1>
  <DataSet=acq_butter4th_bpf.dat>
  <DataDisplay=acq_butter4th_bpf.dpl>
  <OpenDisplay=1>
  <Script=acq_butter4th_bpf.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 940 -110 0 67 0 0 "log" 1 "1MHz" 1 "0.5GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1140 -100 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <Pac P1 1 270 110 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 270 140 0 0 0 0>
  <L L1 1 420 110 8 -26 0 1 "476.5nH" 1 "" 0>
  <C C1 1 390 110 -8 46 0 1 "11.07pF" 1 "" 0 "neutral" 0>
  <GND * 1 420 140 0 0 0 0>
  <L L2 1 530 30 -26 -44 0 0 "66.84nH" 1 "" 0>
  <C C2 1 470 30 -26 10 0 0 "78.96pF" 1 "" 0 "neutral" 0>
  <L L3 1 560 110 8 -26 0 1 "197.4nH" 1 "" 0>
  <C C3 1 530 110 -8 46 0 1 "26.73pF" 1 "" 0 "neutral" 0>
  <GND * 1 560 140 0 0 0 0>
  <L L4 1 670 30 -26 -44 0 0 "27.68nH" 1 "" 0>
  <C C4 1 610 30 -26 10 0 0 "190.6pF" 1 "" 0 "neutral" 0>
  <Pac P2 1 700 110 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 700 140 0 0 0 0>
</Components>
<Wires>
  <270 30 270 80 "" 0 0 0 "">
  <270 30 420 30 "" 0 0 0 "">
  <420 30 420 80 "" 0 0 0 "">
  <560 30 560 80 "" 0 0 0 "">
  <700 30 700 80 "" 0 0 0 "">
  <420 30 440 30 "" 0 0 0 "">
  <390 80 420 80 "" 0 0 0 "">
  <390 140 420 140 "" 0 0 0 "">
  <560 30 580 30 "" 0 0 0 "">
  <530 80 560 80 "" 0 0 0 "">
  <530 140 560 140 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 200 575 620 344 3 #c0c0c0 1 00 1 0 5e+07 5e+08 1 -120 20 10.7099 1 -1 0.5 1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 70 -120 850 890 #000000 0 1 #c0c0c0 1 0>
  <Text 360 -100 16 #000000 0 "Butterworth band-pass filter \n 20MHz...240MHz, pi-type, \n impedance matching 50 Ohm">
</Paintings>
