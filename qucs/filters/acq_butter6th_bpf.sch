<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-387,2119,898,0.826447,35,166>
  <Grid=10,10,1>
  <DataSet=acq_butter6th_bpf.dat>
  <DataDisplay=acq_butter6th_bpf.dpl>
  <OpenDisplay=1>
  <Script=acq_butter6th_bpf.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 940 -110 0 67 0 0 "log" 1 "1MHz" 1 "0.5GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1140 -100 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <GND * 1 210 150 0 0 0 0>
  <L L7 1 360 120 8 -26 0 1 "704.6nH" 1 "" 0>
  <C C7 1 330 120 -8 46 0 1 "7.49pF" 1 "" 0 "neutral" 0>
  <GND * 1 360 150 0 0 0 0>
  <L L8 1 470 40 -26 -44 0 0 "51.15nH" 1 "" 0>
  <C C8 1 410 40 -26 10 0 0 "103.2pF" 1 "" 0 "neutral" 0>
  <L L9 1 500 120 8 -26 0 1 "188.8nH" 1 "" 0>
  <C C9 1 470 120 -8 46 0 1 "27.95pF" 1 "" 0 "neutral" 0>
  <GND * 1 500 150 0 0 0 0>
  <L L10 1 610 40 -26 -44 0 0 "69.88nH" 1 "" 0>
  <C C10 1 550 40 -26 10 0 0 "75.52pF" 1 "" 0 "neutral" 0>
  <L L11 1 640 120 8 -26 0 1 "257.9nH" 1 "" 0>
  <C C11 1 610 120 -8 46 0 1 "20.46pF" 1 "" 0 "neutral" 0>
  <GND * 1 640 150 0 0 0 0>
  <L L12 1 750 40 -26 -44 0 0 "18.72nH" 1 "" 0>
  <C C12 1 690 40 -26 10 0 0 "281.8pF" 1 "" 0 "neutral" 0>
  <GND * 1 780 150 0 0 0 0>
  <Pac P1 1 210 120 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <Pac P2 1 780 120 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
</Components>
<Wires>
  <210 40 210 90 "" 0 0 0 "">
  <210 40 360 40 "" 0 0 0 "">
  <360 40 360 90 "" 0 0 0 "">
  <500 40 500 90 "" 0 0 0 "">
  <640 40 640 90 "" 0 0 0 "">
  <780 40 780 90 "" 0 0 0 "">
  <360 40 380 40 "" 0 0 0 "">
  <330 90 360 90 "" 0 0 0 "">
  <330 150 360 150 "" 0 0 0 "">
  <500 40 520 40 "" 0 0 0 "">
  <470 90 500 90 "" 0 0 0 "">
  <470 150 500 150 "" 0 0 0 "">
  <640 40 660 40 "" 0 0 0 "">
  <610 90 640 90 "" 0 0 0 "">
  <610 150 640 150 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 200 575 620 344 3 #c0c0c0 1 00 1 0 5e+07 5e+08 1 -176.71 50 16.0645 1 -1 0.5 1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 70 -120 850 890 #000000 0 1 #c0c0c0 1 0>
  <Text 360 -100 16 #000000 0 "Butterworth band-pass filter \n 20MHz...240MHz, pi-type, \n impedance matching 50 Ohm">
</Paintings>
