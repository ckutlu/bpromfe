<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-32,1679,1006,0.909091,0,0>
  <Grid=10,10,1>
  <DataSet=acq_cheby3th_lpf.dat>
  <DataDisplay=acq_cheby3th_lpf.dpl>
  <OpenDisplay=1>
  <Script=acq_cheby3th_lpf.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 900 30 0 67 0 0 "log" 1 "1MHz" 1 "0.5GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1100 40 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <Pac P1 1 250 240 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 250 270 0 0 0 0>
  <C C1 1 360 240 17 -26 0 1 "19pF" 1 "" 0 "neutral" 0>
  <GND * 1 360 270 0 0 0 0>
  <L L1 1 430 160 -26 10 0 0 "52.84nH" 1 "" 0>
  <C C2 1 500 240 17 -26 0 1 "19pF" 1 "" 0 "neutral" 0>
  <GND * 1 500 270 0 0 0 0>
  <Pac P2 1 610 240 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 610 270 0 0 0 0>
</Components>
<Wires>
  <250 160 250 210 "" 0 0 0 "">
  <250 160 360 160 "" 0 0 0 "">
  <360 160 360 210 "" 0 0 0 "">
  <500 160 500 210 "" 0 0 0 "">
  <360 160 400 160 "" 0 0 0 "">
  <460 160 500 160 "" 0 0 0 "">
  <610 160 610 210 "" 0 0 0 "">
  <500 160 610 160 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 160 715 620 344 3 #c0c0c0 1 00 1 0 5e+07 5e+08 1 -120 20 10.7099 1 -1 0.5 1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 30 20 850 890 #000000 0 1 #c0c0c0 1 0>
  <Text 240 60 16 #000000 0 "Chebyshev 0.1 dB ripple low-pass filter \n 240MHz cutoff, pi-type, \n impedance matching 50 Ohm">
</Paintings>
