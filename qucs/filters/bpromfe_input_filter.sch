<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-100,1604,1305,0.564474,0,0>
  <Grid=10,10,1>
  <DataSet=bpromfe_input_filter.dat>
  <DataDisplay=bpromfe_input_filter.dpl>
  <OpenDisplay=1>
  <Script=bpromfe_input_filter.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 150 430 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 150 460 0 0 0 0>
  <Eqn Eqn1 1 380 540 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <C C1 1 420 430 17 -26 0 1 "7.958pF" 1 "" 0 "neutral" 0>
  <GND * 1 420 460 0 0 0 0>
  <L L1 1 490 350 -26 10 0 0 "39.79nH" 1 "" 0>
  <C C2 1 560 430 17 -26 0 1 "7.958pF" 1 "" 0 "neutral" 0>
  <GND * 1 560 460 0 0 0 0>
  <Pac P2 1 670 430 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 670 460 0 0 0 0>
  <.SP SP1 1 160 530 0 68 0 0 "log" 1 "1MHz" 1 "0.5 GHz" 1 "1000" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <C C3 1 270 350 -26 17 0 0 "470 pF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <420 350 420 400 "" 0 0 0 "">
  <560 350 560 400 "" 0 0 0 "">
  <420 350 460 350 "" 0 0 0 "">
  <520 350 560 350 "" 0 0 0 "">
  <670 350 670 400 "" 0 0 0 "">
  <560 350 670 350 "" 0 0 0 "">
  <150 350 150 400 "" 0 0 0 "">
  <150 350 240 350 "" 0 0 0 "">
  <300 350 420 350 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 130 1035 571 315 3 #c0c0c0 1 10 1 1e+06 1 1e+09 1 -40 5 5 1 -1 0.2 1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 400 320 260 190 #000000 0 1 #c0c0c0 1 0>
  <Rectangle 230 310 100 120 #000000 0 1 #c0c0c0 1 0>
  <Text 230 280 12 #000000 0 "DC Blocking">
  <Text 440 260 12 #000000 0 "Butterworth low-pass filter \n 400MHz cutoff, pi-type, \n impedance matching 50 Ohm">
  <Text 140 -20 12 #000000 0 "- This is a preselection filter to protect the first amplifier in the \nchain from overloads that may get caused by EMI. \n\n-Since it's a preselection only, it's requirements are relaxed and \nactually has more bandwidth than needed (to reduce the overall \nattenuation in our band of interest due to the filter order).\n\n-Instead of combining a DC block and a low pass filter, \nwe could go for a bandpass filter directly.\nBut this combination provides the user with the flexibility of replacing\nthe DC blocking capacitor with a 0 Ohm jumper. Thus, essentially\nremoving the high pass characteristic at will for experimentation.">
</Paintings>
