<Qucs Schematic 0.0.19>
<Properties>
  <View=0,0,1643,950,1,0,0>
  <Grid=10,10,1>
  <DataSet=acq_bessel6th_lpf.dat>
  <DataDisplay=acq_bessel6th_lpf.dpl>
  <OpenDisplay=1>
  <Script=acq_bessel6th_lpf.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 900 30 0 67 0 0 "log" 1 "1MHz" 1 "0.5GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1100 40 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <Pac P1 1 200 240 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 200 270 0 0 0 0>
  <C C1 1 310 240 17 -26 0 1 "1.81pF" 1 "" 0 "neutral" 0>
  <GND * 1 310 270 0 0 0 0>
  <L L1 1 380 160 -26 10 0 0 "13.27nH" 1 "" 0>
  <C C2 1 450 240 17 -26 0 1 "8.477pF" 1 "" 0 "neutral" 0>
  <GND * 1 450 270 0 0 0 0>
  <L L2 1 520 160 -26 10 0 0 "28.31nH" 1 "" 0>
  <C C3 1 590 240 17 -26 0 1 "14.76pF" 1 "" 0 "neutral" 0>
  <GND * 1 590 270 0 0 0 0>
  <L L3 1 660 160 -26 10 0 0 "75.09nH" 1 "" 0>
  <Pac P2 1 730 240 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 730 270 0 0 0 0>
</Components>
<Wires>
  <200 160 200 210 "" 0 0 0 "">
  <200 160 310 160 "" 0 0 0 "">
  <310 160 310 210 "" 0 0 0 "">
  <450 160 450 210 "" 0 0 0 "">
  <590 160 590 210 "" 0 0 0 "">
  <730 160 730 210 "" 0 0 0 "">
  <310 160 350 160 "" 0 0 0 "">
  <410 160 450 160 "" 0 0 0 "">
  <450 160 490 160 "" 0 0 0 "">
  <550 160 590 160 "" 0 0 0 "">
  <590 160 630 160 "" 0 0 0 "">
  <690 160 730 160 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 160 715 620 344 3 #c0c0c0 1 00 1 0 5e+07 5e+08 1 -120 20 10.7099 1 -1 0.5 1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 30 20 850 890 #000000 0 1 #c0c0c0 1 0>
  <Text 320 40 16 #000000 0 "Bessel low-pass filter \n 240MHz cutoff, pi-type, \n impedance matching 50 Ohm">
</Paintings>
