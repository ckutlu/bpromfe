<Qucs Schematic 0.0.19>
<Properties>
  <View=0,-350,3103,1497,0.51316,0,119>
  <Grid=10,10,1>
  <DataSet=network_before_detection_after_last_amp.dat>
  <DataDisplay=network_before_detection_after_last_amp.dpl>
  <OpenDisplay=1>
  <Script=network_before_detection_after_last_amp.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <.SP SP1 1 930 70 0 69 0 0 "log" 1 "1MHz" 1 "0.5GHz" 1 "201" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 1130 80 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <GND * 1 770 310 0 0 0 0>
  <GND * 1 160 310 0 0 0 0>
  <Pac P1 1 160 280 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <Pac P2 1 770 280 18 -26 0 1 "2" 1 "250 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <R R1 1 700 270 15 -26 0 1 "60 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <C C1 1 360 280 17 -26 0 1 "19pF" 1 "" 0 "neutral" 0>
  <GND * 1 360 310 0 0 0 0>
  <L L1 1 430 200 -26 10 0 0 "52.84nH" 1 "" 0>
  <C C2 1 500 280 17 -26 0 1 "19pF" 1 "" 0 "neutral" 0>
  <GND * 1 500 310 0 0 0 0>
  <C C3 1 600 200 -26 17 0 0 "1.8 nF" 1 "" 0 "neutral" 0>
</Components>
<Wires>
  <770 200 770 250 "" 0 0 0 "">
  <700 200 770 200 "" 0 0 0 "">
  <700 200 700 240 "" 0 0 0 "">
  <700 310 770 310 "" 0 0 0 "">
  <700 300 700 310 "" 0 0 0 "">
  <360 200 360 250 "" 0 0 0 "">
  <500 200 500 250 "" 0 0 0 "">
  <360 200 400 200 "" 0 0 0 "">
  <460 200 500 200 "" 0 0 0 "">
  <160 200 160 250 "" 0 0 0 "">
  <160 200 360 200 "" 0 0 0 "">
  <630 200 700 200 "" 0 0 0 "">
  <500 200 570 200 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 190 755 620 344 3 #c0c0c0 1 00 1 0 5e+07 5e+08 1 -43.9306 10 3.69431 1 -1 0.5 1 315 0 225 "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Rectangle 60 60 850 890 #000000 0 1 #c0c0c0 1 0>
  <Text 270 100 16 #000000 0 "Chebyshev 0.1 dB ripple low-pass filter \n 240MHz cutoff, pi-type, \n impedance matching 50 Ohm">
</Paintings>
