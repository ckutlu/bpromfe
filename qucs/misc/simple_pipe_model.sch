<Qucs Schematic 0.0.19>
<Properties>
  <View=-61,-288,800,1050,0.683014,0,0>
  <Grid=10,10,1>
  <DataSet=simple_pipe_model.dat>
  <DataDisplay=simple_pipe_model.dpl>
  <OpenDisplay=1>
  <Script=simple_pipe_model.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <R R1 1 480 110 15 -26 0 1 "50 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <GND * 1 480 170 0 0 0 0>
  <VProbe Pr1 1 490 -40 28 -31 0 0>
  <GND * 1 500 0 0 0 0 0>
  <COAX Line1 1 410 60 -26 16 0 0 "2.29" 1 "0.022e-6" 0 "1" 0 "2.95 mm" 0 "0.9 mm" 0 "50 mm" 1 "4e-4" 0 "26.85" 0>
  <GND * 1 80 180 0 0 0 0>
  <GND * 1 340 170 0 0 0 0>
  <C C1 1 340 110 17 -26 0 1 "20 pF" 1 "" 0 "neutral" 0>
  <.AC AC1 1 210 260 0 41 0 0 "log" 1 "1 MHz" 1 "500 MHz" 1 "100" 1 "no" 0>
  <IProbe Pr3 1 300 60 -26 16 0 0>
  <IProbe Pr2 1 130 60 -26 16 0 0>
  <Iac I1 1 80 120 20 -26 0 1 "1 mA" 1 "1 GHz" 0 "0" 0 "0" 0>
  <Eqn Eqn1 1 530 300 -28 15 0 0 "TF=Pr1.v/Pr2.i" 1 "yes" 0>
  <C C2 1 210 60 -26 17 0 0 "0.6667 pF" 1 "" 0 "neutral" 0>
  <R R2 1 180 140 15 -26 0 1 "0.1 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
</Components>
<Wires>
  <480 140 480 170 "" 0 0 0 "">
  <340 60 340 80 "" 0 0 0 "">
  <500 -20 500 0 "" 0 0 0 "">
  <340 60 380 60 "" 0 0 0 "">
  <480 -20 480 60 "" 0 0 0 "">
  <480 60 480 80 "" 0 0 0 "">
  <440 60 480 60 "" 0 0 0 "">
  <80 150 80 170 "" 0 0 0 "">
  <340 140 340 170 "" 0 0 0 "">
  <330 60 340 60 "" 0 0 0 "">
  <80 60 80 90 "" 0 0 0 "">
  <80 60 100 60 "" 0 0 0 "">
  <240 60 270 60 "" 0 0 0 "">
  <160 60 180 60 "" 0 0 0 "">
  <180 60 180 110 "" 0 0 0 "">
  <80 170 80 180 "" 0 0 0 "">
  <80 170 180 170 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 30 958 626 518 3 #c0c0c0 1 00 0 0 5e+07 5e+08 1 11.7517 5 53.4718 1 -1 0.2 1 315 0 225 "" "" "">
	<"TF" #0000ff 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Text 110 -180 12 #000000 0 "There is a problem in this one.">
</Paintings>
