EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:detectors
LIBS:rfamps
LIBS:rfconn
LIBS:rfdiodes
LIBS:rftrans
LIBS:opamps
LIBS:bpromfe-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1950 2500 1100 500 
U 58F8D471
F0 "Channel1" 60
F1 "bpromfe_single.sch" 60
F2 "RF_IN" I L 1950 2750 60 
F3 "RF_OUT" I R 3050 2850 60 
F4 "~DSCHG~" I R 3050 2650 60 
$EndSheet
$Comp
L SMA P1
U 1 1 58FB11E9
P 1750 2750
F 0 "P1" H 1760 2870 50  0000 C CNN
F 1 "SMA" V 1860 2690 50  0000 C CNN
F 2 "footprints:CON-SMA-EDGE" H 1750 2750 50  0001 C CNN
F 3 "" H 1750 2750 50  0000 C CNN
	1    1750 2750
	-1   0    0    -1  
$EndComp
$Comp
L SMA P2
U 1 1 58FB1295
P 3250 2850
F 0 "P2" H 3260 2970 50  0000 C CNN
F 1 "SMA" V 3360 2790 50  0000 C CNN
F 2 "footprints:CON-SMA-EDGE" H 3250 2850 50  0001 C CNN
F 3 "" H 3250 2850 50  0000 C CNN
	1    3250 2850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 58FB1347
P 3250 3100
F 0 "#PWR01" H 3250 2850 50  0001 C CNN
F 1 "GND" H 3250 2950 50  0000 C CNN
F 2 "" H 3250 3100 50  0000 C CNN
F 3 "" H 3250 3100 50  0000 C CNN
	1    3250 3100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 58FB1371
P 1750 3000
F 0 "#PWR02" H 1750 2750 50  0001 C CNN
F 1 "GND" H 1750 2850 50  0000 C CNN
F 2 "" H 1750 3000 50  0000 C CNN
F 3 "" H 1750 3000 50  0000 C CNN
	1    1750 3000
	1    0    0    -1  
$EndComp
Text Notes 7050 6750 0    60   ~ 0
8 Channel 20 MHz - 240 MHz Amplifier - Detector Demo/Prototype Board\n for Beam Profile Monitor Front End
Text Notes 7400 7500 0    60   ~ 0
8 Channel RF Amplifier - Detector
Text Notes 8150 7650 0    60   ~ 0
21.04.2017
Text Notes 10600 7650 0    60   ~ 0
1
Text Notes 10200 7500 0    60   ~ 0
Author: Çağlar Kutlu
$Comp
L CONN_01X03 PC1
U 1 1 5900D175
P 2150 6850
F 0 "PC1" H 2150 7050 50  0000 C CNN
F 1 "PWR_IN" V 2250 6850 50  0000 C CNN
F 2 "Connectors:AK300-3" H 2150 6850 50  0001 C CNN
F 3 "" H 2150 6850 50  0000 C CNN
	1    2150 6850
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR03
U 1 1 5900D663
P 2500 6700
F 0 "#PWR03" H 2500 6550 50  0001 C CNN
F 1 "VCC" H 2500 6850 50  0000 C CNN
F 2 "" H 2500 6700 50  0000 C CNN
F 3 "" H 2500 6700 50  0000 C CNN
	1    2500 6700
	1    0    0    -1  
$EndComp
$Comp
L VEE #PWR04
U 1 1 5900D6F6
P 2500 7000
F 0 "#PWR04" H 2500 6850 50  0001 C CNN
F 1 "VEE" H 2500 7150 50  0000 C CNN
F 2 "" H 2500 7000 50  0000 C CNN
F 3 "" H 2500 7000 50  0000 C CNN
	1    2500 7000
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG05
U 1 1 5900D7A1
P 2650 7150
F 0 "#FLG05" H 2650 7245 50  0001 C CNN
F 1 "PWR_FLAG" H 2650 7330 50  0000 C CNN
F 2 "" H 2650 7150 50  0000 C CNN
F 3 "" H 2650 7150 50  0000 C CNN
	1    2650 7150
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG06
U 1 1 5900F134
P 2650 6600
F 0 "#FLG06" H 2650 6695 50  0001 C CNN
F 1 "PWR_FLAG" H 2650 6780 50  0000 C CNN
F 2 "" H 2650 6600 50  0000 C CNN
F 3 "" H 2650 6600 50  0000 C CNN
	1    2650 6600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5900F1C7
P 5600 6900
F 0 "#PWR07" H 5600 6650 50  0001 C CNN
F 1 "GND" H 5600 6750 50  0000 C CNN
F 2 "" H 5600 6900 50  0000 C CNN
F 3 "" H 5600 6900 50  0000 C CNN
	1    5600 6900
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG08
U 1 1 5900F272
P 5600 6700
F 0 "#FLG08" H 5600 6795 50  0001 C CNN
F 1 "PWR_FLAG" H 5600 6880 50  0000 C CNN
F 2 "" H 5600 6700 50  0000 C CNN
F 3 "" H 5600 6700 50  0000 C CNN
	1    5600 6700
	1    0    0    -1  
$EndComp
Text Label 3100 2650 0    60   ~ 0
~DISCHARGE~
$Comp
L BNC DSCHG1
U 1 1 5902C289
P 6050 5550
F 0 "DSCHG1" H 6060 5670 50  0000 C CNN
F 1 "BNC" H 6250 5550 50  0000 C CNN
F 2 "footprints:CON-SMA-EDGE" H 6050 5550 50  0001 C CNN
F 3 "" H 6050 5550 50  0000 C CNN
	1    6050 5550
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR09
U 1 1 5902C39E
P 6050 5850
F 0 "#PWR09" H 6050 5600 50  0001 C CNN
F 1 "GND" H 6050 5700 50  0000 C CNN
F 2 "" H 6050 5850 50  0000 C CNN
F 3 "" H 6050 5850 50  0000 C CNN
	1    6050 5850
	-1   0    0    -1  
$EndComp
Text Label 8250 5450 2    60   ~ 0
~DISCHARGE~
Wire Wire Line
	1900 2750 1950 2750
Wire Wire Line
	1750 2950 1750 3000
Wire Wire Line
	3250 3050 3250 3100
Wire Wire Line
	3100 2850 3050 2850
Wire Wire Line
	2350 6850 5600 6850
Wire Wire Line
	2500 6700 2500 6750
Wire Wire Line
	2350 6750 3150 6750
Wire Wire Line
	2650 6750 2650 6600
Connection ~ 2500 6750
Wire Wire Line
	2350 6950 2750 6950
Wire Wire Line
	2500 6950 2500 7000
Wire Wire Line
	2650 6950 2650 7150
Connection ~ 2500 6950
Wire Wire Line
	3100 2650 3050 2650
Wire Wire Line
	6050 5750 6050 5850
$Comp
L AD8011 U1
U 1 1 5903CE19
P 7050 5450
F 0 "U1" H 7100 5250 50  0000 L CNN
F 1 "AD8011" H 7100 5350 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 7100 5500 50  0001 C CNN
F 3 "" H 7200 5600 50  0000 C CNN
	1    7050 5450
	1    0    0    -1  
$EndComp
$Comp
L R RF1
U 1 1 5903CE20
P 7050 5000
F 0 "RF1" V 6950 5000 50  0000 C CNN
F 1 "1k" V 7050 5000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6980 5000 50  0001 C CNN
F 3 "" H 7050 5000 50  0000 C CNN
	1    7050 5000
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR010
U 1 1 5903CE27
P 6950 5150
F 0 "#PWR010" H 6950 5000 50  0001 C CNN
F 1 "VCC" H 6950 5300 50  0000 C CNN
F 2 "" H 6950 5150 50  0000 C CNN
F 3 "" H 6950 5150 50  0000 C CNN
	1    6950 5150
	0    1    1    0   
$EndComp
$Comp
L VEE #PWR011
U 1 1 5903CE2D
P 6950 5750
F 0 "#PWR011" H 6950 5600 50  0001 C CNN
F 1 "VEE" H 6950 5900 50  0000 C CNN
F 2 "" H 6950 5750 50  0000 C CNN
F 3 "" H 6950 5750 50  0000 C CNN
	1    6950 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7350 5450 7350 5000
Wire Wire Line
	7350 5000 7200 5000
$Comp
L R RG1
U 1 1 5903CE35
P 6600 5350
F 0 "RG1" V 6680 5350 50  0000 C CNN
F 1 "1k" V 6600 5350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6530 5350 50  0001 C CNN
F 3 "" H 6600 5350 50  0000 C CNN
	1    6600 5350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6900 5000 6750 5000
Wire Wire Line
	6750 5000 6750 5350
$Comp
L R RO1
U 1 1 5903CE3E
P 7550 5450
F 0 "RO1" V 7630 5450 50  0000 C CNN
F 1 "42.2" V 7550 5450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7480 5450 50  0001 C CNN
F 3 "" H 7550 5450 50  0000 C CNN
	1    7550 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 5450 7350 5450
Text Notes 6450 5200 0    15   ~ 0
RG to (-) path must be short!
$Comp
L R RS1
U 1 1 5903CE4E
P 6550 5750
F 0 "RS1" V 6630 5750 50  0000 C CNN
F 1 "50" V 6550 5750 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6480 5750 50  0001 C CNN
F 3 "" H 6550 5750 50  0000 C CNN
	1    6550 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5550 6750 5550
Wire Wire Line
	6550 5600 6550 5550
Connection ~ 6550 5550
$Comp
L GND #PWR012
U 1 1 5903CE59
P 6450 5400
F 0 "#PWR012" H 6450 5150 50  0001 C CNN
F 1 "GND" H 6350 5400 50  0000 C CNN
F 2 "" H 6450 5400 50  0000 C CNN
F 3 "" H 6450 5400 50  0000 C CNN
	1    6450 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 5350 6450 5400
Wire Wire Line
	7700 5450 8250 5450
$Comp
L GND #PWR013
U 1 1 59041F3D
P 6550 5950
F 0 "#PWR013" H 6550 5700 50  0001 C CNN
F 1 "GND" H 6550 5800 50  0000 C CNN
F 2 "" H 6550 5950 50  0000 C CNN
F 3 "" H 6550 5950 50  0000 C CNN
	1    6550 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 5950 6550 5900
Wire Notes Line
	6250 4850 6250 6200
Wire Notes Line
	6250 6200 8350 6200
Wire Notes Line
	8350 6200 8350 4800
Wire Notes Line
	8350 4800 6250 4800
Wire Notes Line
	6250 4800 6250 4900
Text Notes 6950 4800 0    60   ~ 0
Buffer Amplifier
Text Notes 8500 5500 0    60   ~ 0
- Select RO for the input capacitance \nseen from the DISCHARGE port. \nHigher RO for Higher C\n- Don't install RG1 for unity gain!\n--- I've put a network in front of DISCHARGE\n JFETs to suppress peaking if necessary. If using them\nRO1 is probably unnecessary.
$Comp
L CP1 CR1
U 1 1 590269D9
P 3300 6650
F 0 "CR1" H 3325 6750 50  0000 L CNN
F 1 "100u" H 3325 6550 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 3300 6650 50  0001 C CNN
F 3 "" H 3300 6650 50  0000 C CNN
	1    3300 6650
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR2
U 1 1 59026C6B
P 3300 7050
F 0 "CR2" H 3325 7150 50  0000 L CNN
F 1 "100u" H 3325 6950 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 3300 7050 50  0001 C CNN
F 3 "" H 3300 7050 50  0000 C CNN
	1    3300 7050
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR3
U 1 1 59026E73
P 3700 6650
F 0 "CR3" H 3725 6750 50  0000 L CNN
F 1 "100u" H 3725 6550 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 3700 6650 50  0001 C CNN
F 3 "" H 3700 6650 50  0000 C CNN
	1    3700 6650
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR4
U 1 1 59026E79
P 3700 7050
F 0 "CR4" H 3725 7150 50  0000 L CNN
F 1 "100u" H 3725 6950 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 3700 7050 50  0001 C CNN
F 3 "" H 3700 7050 50  0000 C CNN
	1    3700 7050
	1    0    0    -1  
$EndComp
$Comp
L C CBL1
U 1 1 59026E97
P 4100 6650
F 0 "CBL1" H 4125 6750 50  0000 L CNN
F 1 "100p" H 4125 6550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4138 6500 50  0001 C CNN
F 3 "" H 4100 6650 50  0000 C CNN
	1    4100 6650
	1    0    0    -1  
$EndComp
$Comp
L C CBL2
U 1 1 59026F06
P 4100 7050
F 0 "CBL2" H 4125 7150 50  0000 L CNN
F 1 "100p" H 4125 6950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4138 6900 50  0001 C CNN
F 3 "" H 4100 7050 50  0000 C CNN
	1    4100 7050
	1    0    0    -1  
$EndComp
$Comp
L C CBH1
U 1 1 59026F61
P 4450 6650
F 0 "CBH1" H 4475 6750 50  0000 L CNN
F 1 "0.1u" H 4475 6550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4488 6500 50  0001 C CNN
F 3 "" H 4450 6650 50  0000 C CNN
	1    4450 6650
	1    0    0    -1  
$EndComp
$Comp
L C CBH2
U 1 1 59026FC1
P 4450 7050
F 0 "CBH2" H 4475 7150 50  0000 L CNN
F 1 "0.1u" H 4475 6950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4488 6900 50  0001 C CNN
F 3 "" H 4450 7050 50  0000 C CNN
	1    4450 7050
	1    0    0    -1  
$EndComp
$Comp
L C CBL3
U 1 1 59027124
P 4800 6650
F 0 "CBL3" H 4825 6750 50  0000 L CNN
F 1 "100p" H 4825 6550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4838 6500 50  0001 C CNN
F 3 "" H 4800 6650 50  0000 C CNN
	1    4800 6650
	1    0    0    -1  
$EndComp
$Comp
L C CBL4
U 1 1 5902712A
P 4800 7050
F 0 "CBL4" H 4825 7150 50  0000 L CNN
F 1 "100p" H 4825 6950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4838 6900 50  0001 C CNN
F 3 "" H 4800 7050 50  0000 C CNN
	1    4800 7050
	1    0    0    -1  
$EndComp
$Comp
L C CBH3
U 1 1 59027130
P 5150 6650
F 0 "CBH3" H 5175 6750 50  0000 L CNN
F 1 "0.1u" H 5175 6550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5188 6500 50  0001 C CNN
F 3 "" H 5150 6650 50  0000 C CNN
	1    5150 6650
	1    0    0    -1  
$EndComp
$Comp
L C CBH4
U 1 1 59027136
P 5150 7050
F 0 "CBH4" H 5175 7150 50  0000 L CNN
F 1 "0.1u" H 5175 6950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5188 6900 50  0001 C CNN
F 3 "" H 5150 7050 50  0000 C CNN
	1    5150 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 6750 3150 6450
Wire Wire Line
	3150 6450 5150 6450
Wire Wire Line
	5150 6450 5150 6500
Connection ~ 2650 6750
Wire Wire Line
	4800 6500 4800 6450
Connection ~ 4800 6450
Wire Wire Line
	4450 6500 4450 6450
Connection ~ 4450 6450
Wire Wire Line
	4100 6500 4100 6450
Connection ~ 4100 6450
Wire Wire Line
	3700 6500 3700 6450
Connection ~ 3700 6450
Wire Wire Line
	3300 6500 3300 6450
Connection ~ 3300 6450
Wire Wire Line
	5150 6800 5150 6900
Connection ~ 5150 6850
Wire Wire Line
	4800 6800 4800 6900
Connection ~ 4800 6850
Wire Wire Line
	4450 6800 4450 6900
Connection ~ 4450 6850
Wire Wire Line
	4100 6800 4100 6900
Connection ~ 4100 6850
Wire Wire Line
	3700 6800 3700 6900
Connection ~ 3700 6850
Wire Wire Line
	3300 6800 3300 6900
Connection ~ 3300 6850
Wire Wire Line
	3300 7200 3300 7250
Wire Wire Line
	3150 7250 5150 7250
Wire Wire Line
	5150 7250 5150 7200
Wire Wire Line
	4800 7200 4800 7250
Connection ~ 4800 7250
Wire Wire Line
	4450 7200 4450 7250
Connection ~ 4450 7250
Wire Wire Line
	4100 7200 4100 7250
Connection ~ 4100 7250
Wire Wire Line
	3700 7200 3700 7250
Connection ~ 3700 7250
Wire Wire Line
	3150 7250 3150 7100
Wire Wire Line
	3150 7100 2750 7100
Wire Wire Line
	2750 7100 2750 6950
Connection ~ 2650 6950
Connection ~ 3300 7250
Wire Wire Line
	5600 6700 5600 6900
Connection ~ 5600 6850
$EndSCHEMATC
