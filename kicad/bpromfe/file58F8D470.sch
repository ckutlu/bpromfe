EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:bpromfe-cache
LIBS:amp_blocks-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BGA427 U?
U 1 1 58F98E15
P 2150 1500
F 0 "U?" H 2150 1650 50  0000 L CNN
F 1 "BGA427" H 2150 1350 50  0000 L CNN
F 2 "" H 2050 1550 50  0000 C CNN
F 3 "" H 2150 1650 50  0000 C CNN
	1    2150 1500
	1    0    0    -1  
$EndComp
$Comp
L BGA2851 U?
U 1 1 58F98E16
P 5450 1500
F 0 "U?" H 5450 1650 50  0000 L CNN
F 1 "BGA2851" H 5450 1350 50  0000 L CNN
F 2 "" H 5350 1550 50  0000 C CNN
F 3 "" H 5450 1650 50  0000 C CNN
	1    5450 1500
	1    0    0    -1  
$EndComp
$Comp
L BGA2869 U?
U 1 1 58F98E17
P 3600 1500
F 0 "U?" H 3600 1650 50  0000 L CNN
F 1 "BGA2869" H 3600 1350 50  0000 L CNN
F 2 "" H 3500 1550 50  0000 C CNN
F 3 "" H 3600 1650 50  0000 C CNN
	1    3600 1500
	1    0    0    -1  
$EndComp
$Comp
L LTC5507 U?
U 1 1 58F98E18
P 2750 4650
F 0 "U?" H 2750 4400 60  0000 C CNN
F 1 "LTC5507" H 2750 4850 60  0000 C CNN
F 2 "" H 2550 4650 60  0001 C CNN
F 3 "" H 2550 4650 60  0001 C CNN
	1    2750 4650
	-1   0    0    1   
$EndComp
$Comp
L C C?
U 1 1 58F98E19
P 2050 2800
F 0 "C?" V 2200 2750 50  0000 L CNN
F 1 "470 pF" V 1900 2650 50  0000 L CNN
F 2 "" H 2088 2650 50  0000 C CNN
F 3 "" H 2050 2800 50  0000 C CNN
	1    2050 2800
	0    -1   -1   0   
$EndComp
$Comp
L C C?
U 1 1 58F98E1A
P 2400 3000
F 0 "C?" H 2425 3100 50  0000 L CNN
F 1 "7.958 pF" H 2425 2900 50  0000 L CNN
F 2 "" H 2438 2850 50  0000 C CNN
F 3 "" H 2400 3000 50  0000 C CNN
	1    2400 3000
	1    0    0    -1  
$EndComp
Text Notes 7350 2450 0    60   ~ 0
Power Gains of Amps ( Typical Values )\n-----------------------\nBGA427   :    27 dB\nBGA2851 :    23.2 dB\nBGA2869 :    31.1 dB
$Comp
L VCC #PWR01
U 1 1 58F98E1B
P 2050 1200
F 0 "#PWR01" H 2050 1050 50  0001 C CNN
F 1 "VCC" H 2050 1350 50  0000 C CNN
F 2 "" H 2050 1200 50  0000 C CNN
F 3 "" H 2050 1200 50  0000 C CNN
	1    2050 1200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 58F98E1C
P 3500 1200
F 0 "#PWR02" H 3500 1050 50  0001 C CNN
F 1 "VCC" H 3500 1350 50  0000 C CNN
F 2 "" H 3500 1200 50  0000 C CNN
F 3 "" H 3500 1200 50  0000 C CNN
	1    3500 1200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR03
U 1 1 58F98E1D
P 5350 1200
F 0 "#PWR03" H 5350 1050 50  0001 C CNN
F 1 "VCC" H 5350 1350 50  0000 C CNN
F 2 "" H 5350 1200 50  0000 C CNN
F 3 "" H 5350 1200 50  0000 C CNN
	1    5350 1200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 58F98E1E
P 2050 1800
F 0 "#PWR04" H 2050 1550 50  0001 C CNN
F 1 "GND" H 2050 1650 50  0000 C CNN
F 2 "" H 2050 1800 50  0000 C CNN
F 3 "" H 2050 1800 50  0000 C CNN
	1    2050 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 58F98E1F
P 3500 1800
F 0 "#PWR05" H 3500 1550 50  0001 C CNN
F 1 "GND" H 3500 1650 50  0000 C CNN
F 2 "" H 3500 1800 50  0000 C CNN
F 3 "" H 3500 1800 50  0000 C CNN
	1    3500 1800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 58F98E20
P 5350 1800
F 0 "#PWR06" H 5350 1550 50  0001 C CNN
F 1 "GND" H 5350 1650 50  0000 C CNN
F 2 "" H 5350 1800 50  0000 C CNN
F 3 "" H 5350 1800 50  0000 C CNN
	1    5350 1800
	1    0    0    -1  
$EndComp
Text GLabel 1750 1500 0    60   Input ~ 0
AMPBLK_IN
Text GLabel 5850 1500 2    60   Input ~ 0
AMPBLK_OUT
Text Notes 2700 850  0    60   ~ 0
AMPLIFIER BLOCK
$Comp
L C C?
U 1 1 58F98E21
P 3100 3000
F 0 "C?" H 3125 3100 50  0000 L CNN
F 1 "7.958 pF" H 3125 2900 50  0000 L CNN
F 2 "" H 3138 2850 50  0000 C CNN
F 3 "" H 3100 3000 50  0000 C CNN
	1    3100 3000
	1    0    0    -1  
$EndComp
$Comp
L L L?
U 1 1 58F98E22
P 2750 2800
F 0 "L?" V 2700 2800 50  0000 C CNN
F 1 "39.79nH" V 2825 2800 50  0000 C CNN
F 2 "" H 2750 2800 50  0000 C CNN
F 3 "" H 2750 2800 50  0000 C CNN
	1    2750 2800
	0    1    1    0   
$EndComp
$Comp
L GND #PWR07
U 1 1 58F98E23
P 2400 3150
F 0 "#PWR07" H 2400 2900 50  0001 C CNN
F 1 "GND" H 2400 3000 50  0000 C CNN
F 2 "" H 2400 3150 50  0000 C CNN
F 3 "" H 2400 3150 50  0000 C CNN
	1    2400 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 58F98E24
P 3100 3150
F 0 "#PWR08" H 3100 2900 50  0001 C CNN
F 1 "GND" H 3100 3000 50  0000 C CNN
F 2 "" H 3100 3150 50  0000 C CNN
F 3 "" H 3100 3150 50  0000 C CNN
	1    3100 3150
	1    0    0    -1  
$EndComp
Text GLabel 1850 2800 0    60   Input ~ 0
PRE_FILT_IN
Text GLabel 3300 2800 2    60   Input ~ 0
PRE_FILT_OUT
Text Notes 2100 2500 0    60   ~ 0
Preselection Filter
Text Notes 1400 3600 0    60   ~ 0
 (s-parameters @ qucs "bpromfe_input_filter.sch")
Text Notes 7350 1900 0    60   Italic 0
Notes On Amplifier Block\n\n========================\n- PCB layout must be made so that CBx and RBx have the same footprint.\n- If you want to bypass BGA2869 :\n    --> Don't solder CB1, solder 0 Ohm to RB1 and RB2, solder CB2\n- RBx are 0 Ohm (smt jumper)\n- All C_DCx can also be 0 Ohm components depending on the experimentation\nmeasurements. Otherwise, they should be chosen as DC blocking (high-pass)\ncapacitors for passing above approx. 1 MHz. 100 -560 pF is a good range. \nPick 100 pF if in doubt. That corresponds to a 20 MHz cut-off for the 1st order\nRC filter formed with the input resistance of the amplifier in front.\n- One can go for a second order low pass in front of BGA2851. Which would\nintroduce an inductor for maybe not so much reward in return. \n
$Comp
L R RB?
U 1 1 58F98E25
P 2750 1700
F 0 "RB?" V 2830 1700 50  0000 C CNN
F 1 "0" H 2650 1800 50  0000 C CNN
F 2 "" V 2680 1700 50  0000 C CNN
F 3 "" H 2750 1700 50  0000 C CNN
	1    2750 1700
	-1   0    0    1   
$EndComp
$Comp
L C CB?
U 1 1 58F98E26
P 3000 1500
F 0 "CB?" V 3150 1500 50  0000 L CNN
F 1 "100 pF" V 2850 1350 50  0000 L CNN
F 2 "" H 3038 1350 50  0000 C CNN
F 3 "" H 3000 1500 50  0000 C CNN
	1    3000 1500
	0    -1   -1   0   
$EndComp
$Comp
L R RB?
U 1 1 58F98E27
P 4250 1750
F 0 "RB?" V 4330 1750 50  0000 C CNN
F 1 "0" H 4150 1850 50  0000 C CNN
F 2 "" V 4180 1750 50  0000 C CNN
F 3 "" H 4250 1750 50  0000 C CNN
	1    4250 1750
	-1   0    0    1   
$EndComp
$Comp
L C CB?
U 1 1 58F98E28
P 4750 1500
F 0 "CB?" V 4900 1450 50  0000 L CNN
F 1 "100 pF" V 4600 1350 50  0000 L CNN
F 2 "" H 4788 1350 50  0000 C CNN
F 3 "" H 4750 1500 50  0000 C CNN
	1    4750 1500
	0    -1   -1   0   
$EndComp
Text Label 3200 2100 0    60   Italic 0
AMPBLK_BYP1
$Comp
L C C?
U 1 1 58F98E29
P 5600 3000
F 0 "C?" H 5625 3100 50  0000 L CNN
F 1 "19 pF" H 5625 2900 50  0000 L CNN
F 2 "" H 5638 2850 50  0000 C CNN
F 3 "" H 5600 3000 50  0000 C CNN
	1    5600 3000
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58F98E2A
P 6300 3000
F 0 "C?" H 6325 3100 50  0000 L CNN
F 1 "19 pF" H 6325 2900 50  0000 L CNN
F 2 "" H 6338 2850 50  0000 C CNN
F 3 "" H 6300 3000 50  0000 C CNN
	1    6300 3000
	1    0    0    -1  
$EndComp
$Comp
L L L?
U 1 1 58F98E2B
P 5950 2800
F 0 "L?" V 5900 2800 50  0000 C CNN
F 1 "52.84 nH" V 6025 2800 50  0000 C CNN
F 2 "" H 5950 2800 50  0000 C CNN
F 3 "" H 5950 2800 50  0000 C CNN
	1    5950 2800
	0    1    1    0   
$EndComp
$Comp
L GND #PWR09
U 1 1 58F98E2C
P 5600 3150
F 0 "#PWR09" H 5600 2900 50  0001 C CNN
F 1 "GND" H 5600 3000 50  0000 C CNN
F 2 "" H 5600 3150 50  0000 C CNN
F 3 "" H 5600 3150 50  0000 C CNN
	1    5600 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 58F98E2D
P 6300 3150
F 0 "#PWR010" H 6300 2900 50  0001 C CNN
F 1 "GND" H 6300 3000 50  0000 C CNN
F 2 "" H 6300 3150 50  0000 C CNN
F 3 "" H 6300 3150 50  0000 C CNN
	1    6300 3150
	1    0    0    -1  
$EndComp
Text GLabel 5200 2800 0    60   Input ~ 0
ACQ_FILT_IN
Text GLabel 6500 2800 2    60   Input ~ 0
ACQ_FILT_OUT
Text Notes 5450 2500 0    60   ~ 0
Acquisition Filter
Text Notes 4600 3600 0    60   ~ 0
 (s-parameters @ qucs "bpromfe_acquisition_filter.sch")
Text Notes 600  7700 0    30   Italic 0
NOTE ON LTC5507 INPUT MATCHING\n==========================\n- The RF_IN port of the LTC5507 is 250 Ohm. Simple 60 Ohm resistor would\nmake an 'okay' match to prevent reflections. S21 without resistor is around \n\n-3 dB, with resistor -7 dB. |S21|^2=> -14 dB. Considering we match, this adds\n NF_4 = 14 dB. Calculating total NF with 3 gain and 1 attenuation due to \nmismatch stage (nfOfChain.m). NF only degrades 0.015.  Of course we lose 14 dB \nin power.\n\n- If wanted the matching resistor could be left unconnected, corresponding to\nmore reflected power (S11 = -5 dB) if the previous stage could handle that.\n\n- An ideal case would be to use a step-up transformer or a un-un, but I never\nused any transformer in this frequency range. This can be investigated.
$Comp
L C C_L?
U 1 1 58F98E2E
P 1900 4750
F 0 "C_L?" V 1800 4600 50  0000 L CNN
F 1 "1.8 nF" V 2050 4650 50  0000 L CNN
F 2 "" H 1938 4600 50  0000 C CNN
F 3 "" H 1900 4750 50  0000 C CNN
	1    1900 4750
	0    1    1    0   
$EndComp
Text Notes 1200 5750 0    60   Italic 0
NOTES ON LTC5507\n============\n- C_L2 selected to have corner freq. at 20 MHz (see datasheet). \n- See input matching notes at the corner\n- The output bandwidth should be around 150 kHz.\n- According to datasheet there is a 250 mV typ. DC offset \nat the LTC output.
$Comp
L C_Small C_L?
U 1 1 58F98E2F
P 2150 4550
F 0 "C_L?" V 2050 4500 50  0000 L CNN
F 1 "1.8nF" V 2100 4250 50  0000 L CNN
F 2 "" H 2150 4550 50  0000 C CNN
F 3 "" H 2150 4550 50  0000 C CNN
	1    2150 4550
	0    1    1    0   
$EndComp
Text GLabel 1750 4750 0    60   Input Italic 0
DET_LTC_IN
$Comp
L GND #PWR011
U 1 1 58F98E30
P 3400 4700
F 0 "#PWR011" H 3400 4450 50  0001 C CNN
F 1 "GND" H 3400 4550 50  0000 C CNN
F 2 "" H 3400 4700 50  0000 C CNN
F 3 "" H 3400 4700 50  0000 C CNN
	1    3400 4700
	1    0    0    -1  
$EndComp
$Comp
L R R?
U 1 1 58F98E31
P 3050 5100
F 0 "R?" V 3130 5100 50  0000 C CNN
F 1 "22k" V 3050 5100 50  0000 C CNN
F 2 "" V 2980 5100 50  0000 C CNN
F 3 "" H 3050 5100 50  0000 C CNN
	1    3050 5100
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR012
U 1 1 58F98E32
P 2900 5100
F 0 "#PWR012" H 2900 4950 50  0001 C CNN
F 1 "VCC" H 2900 5250 50  0000 C CNN
F 2 "" H 2900 5100 50  0000 C CNN
F 3 "" H 2900 5100 50  0000 C CNN
	1    2900 5100
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NO_Small JP?
U 1 1 58F98E33
P 3400 5100
F 0 "JP?" H 3400 5180 50  0000 C CNN
F 1 "JP_SHDN" H 3410 5040 50  0000 C CNN
F 2 "" H 3400 5100 50  0000 C CNN
F 3 "" H 3400 5100 50  0000 C CNN
	1    3400 5100
	1    0    0    -1  
$EndComp
Text GLabel 3550 4550 2    60   Input Italic 0
DET_LTC_OUT
$Comp
L VCC #PWR013
U 1 1 58F98E34
P 2300 4350
F 0 "#PWR013" H 2300 4200 50  0001 C CNN
F 1 "VCC" H 2300 4500 50  0000 C CNN
F 2 "" H 2300 4350 50  0000 C CNN
F 3 "" H 2300 4350 50  0000 C CNN
	1    2300 4350
	1    0    0    -1  
$EndComp
Text Notes 2300 4050 0    60   Italic 0
Peak Detector
Text Notes 1200 3350 0    60   Italic 0
400 MHz LPF Butter\nand DC Block
Text Notes 4400 3300 0    60   Italic 0
240 MHz \n0.1 dB ripple \nLPF Chebyshev
$Comp
L R RM?
U 1 1 58F98E35
P 2250 4950
F 0 "RM?" V 2330 4950 50  0000 C CNN
F 1 "60" V 2250 4950 50  0000 C CNN
F 2 "" V 2180 4950 50  0000 C CNN
F 3 "" H 2250 4950 50  0000 C CNN
	1    2250 4950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 58F98E36
P 2250 5100
F 0 "#PWR014" H 2250 4850 50  0001 C CNN
F 1 "GND" H 2250 4950 50  0000 C CNN
F 2 "" H 2250 5100 50  0000 C CNN
F 3 "" H 2250 5100 50  0000 C CNN
	1    2250 5100
	1    0    0    -1  
$EndComp
Text GLabel 5400 4550 0    60   Input ~ 0
RF_IN
Text GLabel 5400 4750 0    60   Input ~ 0
RF_OUT
$Comp
L SMA P_IN?
U 1 1 58F98E37
P 5950 5100
F 0 "P_IN?" H 5960 5220 50  0000 C CNN
F 1 "SMA" V 6060 5040 50  0000 C CNN
F 2 "" H 5950 5100 50  0000 C CNN
F 3 "" H 5950 5100 50  0000 C CNN
	1    5950 5100
	-1   0    0    -1  
$EndComp
$Comp
L SMA P_OUT?
U 1 1 58F98E38
P 7150 5100
F 0 "P_OUT?" H 7160 5220 50  0000 C CNN
F 1 "SMA" V 7260 5040 50  0000 C CNN
F 2 "" H 7150 5100 50  0000 C CNN
F 3 "" H 7150 5100 50  0000 C CNN
	1    7150 5100
	1    0    0    -1  
$EndComp
Text GLabel 6150 5100 2    60   Input ~ 0
RF_IN
Text GLabel 6950 5100 0    60   Input ~ 0
RF_OUT
$Comp
L GND #PWR015
U 1 1 58F98E39
P 5950 5300
F 0 "#PWR015" H 5950 5050 50  0001 C CNN
F 1 "GND" H 5950 5150 50  0000 C CNN
F 2 "" H 5950 5300 50  0000 C CNN
F 3 "" H 5950 5300 50  0000 C CNN
	1    5950 5300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 58F98E3A
P 7150 5300
F 0 "#PWR016" H 7150 5050 50  0001 C CNN
F 1 "GND" H 7150 5150 50  0000 C CNN
F 2 "" H 7150 5300 50  0000 C CNN
F 3 "" H 7150 5300 50  0000 C CNN
	1    7150 5300
	1    0    0    -1  
$EndComp
Text GLabel 6800 4550 2    60   Input ~ 0
AMPBLK_IN
Text GLabel 8000 4550 0    60   Input ~ 0
AMPBLK_OUT
Text GLabel 5450 4550 2    60   Input ~ 0
PRE_FILT_IN
Text GLabel 6750 4550 0    60   Input ~ 0
PRE_FILT_OUT
Text GLabel 8050 4750 0    60   Input ~ 0
ACQ_FILT_IN
Text GLabel 6750 4750 2    60   Input ~ 0
ACQ_FILT_OUT
Text GLabel 6700 4750 0    60   Input Italic 0
DET_LTC_IN
$Comp
L GND #PWR017
U 1 1 58F98E3B
P 3600 5100
F 0 "#PWR017" H 3600 4850 50  0001 C CNN
F 1 "GND" H 3600 4950 50  0000 C CNN
F 2 "" H 3600 5100 50  0000 C CNN
F 3 "" H 3600 5100 50  0000 C CNN
	1    3600 5100
	1    0    0    -1  
$EndComp
Text GLabel 5450 4750 2    60   Input Italic 0
DET_LTC_OUT
Text Notes 7150 6750 0    60   ~ 0
This is the schematic of a single detection block. This is not a final design.
Text Notes 7400 7500 0    60   ~ 0
Amplifier Blocks for BPROMFE (Beam Profile Monitor Front-End)
Text Notes 8150 7650 0    60   ~ 0
04.20.2017
Text Notes 10600 7650 0    60   ~ 0
0.1
$Comp
L GND #PWR018
U 1 1 58F98E3C
P 3200 6800
F 0 "#PWR018" H 3200 6550 50  0001 C CNN
F 1 "GND" H 3200 6650 50  0000 C CNN
F 2 "" H 3200 6800 50  0000 C CNN
F 3 "" H 3200 6800 50  0000 C CNN
	1    3200 6800
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR019
U 1 1 58F98E3D
P 3200 6550
F 0 "#PWR019" H 3200 6400 50  0001 C CNN
F 1 "VCC" H 3200 6700 50  0000 C CNN
F 2 "" H 3200 6550 50  0000 C CNN
F 3 "" H 3200 6550 50  0000 C CNN
	1    3200 6550
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG020
U 1 1 58F98E3E
P 3500 6600
F 0 "#FLG020" H 3500 6695 50  0001 C CNN
F 1 "PWR_FLAG" H 3500 6780 50  0000 C CNN
F 2 "" H 3500 6600 50  0000 C CNN
F 3 "" H 3500 6600 50  0000 C CNN
	1    3500 6600
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG021
U 1 1 58F98E3F
P 3500 6700
F 0 "#FLG021" H 3500 6795 50  0001 C CNN
F 1 "PWR_FLAG" H 3500 6880 50  0000 C CNN
F 2 "" H 3500 6700 50  0000 C CNN
F 3 "" H 3500 6700 50  0000 C CNN
	1    3500 6700
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P?
U 1 1 58F98E40
P 2900 6650
F 0 "P?" H 2900 6800 50  0000 C CNN
F 1 "PWR_IN" V 3000 6650 50  0000 C CNN
F 2 "" H 2900 6650 50  0000 C CNN
F 3 "" H 2900 6650 50  0000 C CNN
	1    2900 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	5350 1800 5450 1800
Wire Wire Line
	3600 1800 3500 1800
Wire Wire Line
	5750 1500 5850 1500
Wire Notes Line
	1100 900  6550 900 
Wire Wire Line
	2200 2800 2600 2800
Wire Wire Line
	2400 2850 2400 2800
Connection ~ 2400 2800
Wire Wire Line
	2900 2800 3300 2800
Wire Wire Line
	3100 2800 3100 2850
Wire Wire Line
	1900 2800 1850 2800
Connection ~ 3100 2800
Wire Notes Line
	1100 2550 1100 3450
Wire Notes Line
	1100 3450 4100 3450
Wire Notes Line
	4100 3450 4100 2550
Wire Notes Line
	4100 2550 1100 2550
Wire Wire Line
	4250 2100 4250 1900
Wire Notes Line
	6550 900  6550 2300
Wire Notes Line
	6550 2300 1100 2300
Wire Notes Line
	1100 2300 1100 900 
Wire Wire Line
	2750 2100 4250 2100
Wire Wire Line
	2750 2100 2750 1850
Wire Wire Line
	2450 1500 2850 1500
Wire Wire Line
	3150 1500 3300 1500
Wire Wire Line
	2750 1550 2750 1500
Connection ~ 2750 1500
Wire Wire Line
	4900 1500 5150 1500
Wire Wire Line
	3900 1500 4600 1500
Wire Wire Line
	4250 1600 4250 1500
Connection ~ 4250 1500
Wire Wire Line
	3200 2100 3200 2100
Connection ~ 3200 2100
Wire Wire Line
	5200 2800 5800 2800
Wire Wire Line
	5600 2850 5600 2800
Connection ~ 5600 2800
Wire Wire Line
	6100 2800 6500 2800
Wire Wire Line
	6300 2800 6300 2850
Connection ~ 6300 2800
Wire Notes Line
	4300 2550 4300 3450
Wire Notes Line
	4300 3450 7300 3450
Wire Notes Line
	7300 3450 7300 2550
Wire Notes Line
	7300 2550 4300 2550
Wire Wire Line
	2000 4550 2000 4650
Wire Wire Line
	2050 4750 2300 4750
Wire Wire Line
	3200 4650 3400 4650
Wire Wire Line
	3400 4650 3400 4700
Wire Wire Line
	3200 4750 3250 4750
Wire Wire Line
	3250 4750 3250 5100
Wire Wire Line
	3200 5100 3300 5100
Connection ~ 3250 5100
Wire Wire Line
	3600 5100 3500 5100
Wire Wire Line
	2000 4650 2300 4650
Wire Wire Line
	2250 4550 2300 4550
Wire Wire Line
	2050 4550 2000 4550
Wire Wire Line
	2300 4550 2300 4350
Wire Notes Line
	1100 4100 4300 4100
Wire Notes Line
	4300 4100 4300 5800
Wire Notes Line
	4300 5800 1100 5800
Wire Notes Line
	1100 5800 1100 4100
Wire Wire Line
	2250 4800 2250 4750
Connection ~ 2250 4750
Wire Wire Line
	3100 6700 3600 6700
Wire Wire Line
	3200 6700 3200 6800
Wire Wire Line
	3200 4550 3550 4550
Wire Wire Line
	1850 1500 1750 1500
Connection ~ 3200 6700
Wire Wire Line
	5400 4550 5450 4550
Wire Wire Line
	6750 4550 6800 4550
Wire Wire Line
	8000 4550 8200 4550
Wire Wire Line
	8200 4550 8200 4750
Wire Wire Line
	8200 4750 8050 4750
Wire Wire Line
	6700 4750 6750 4750
Wire Wire Line
	5450 4750 5400 4750
Wire Wire Line
	6150 5100 6100 5100
Wire Wire Line
	6950 5100 7000 5100
$Comp
L C C?
U 1 1 58F98E41
P 4150 6650
F 0 "C?" H 4175 6750 50  0000 L CNN
F 1 "0.1uF" H 4200 6550 50  0000 L CNN
F 2 "" H 4188 6500 50  0000 C CNN
F 3 "" H 4150 6650 50  0000 C CNN
	1    4150 6650
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58F98E42
P 4450 6650
F 0 "C?" H 4475 6750 50  0000 L CNN
F 1 "100pF" H 4475 6550 50  0000 L CNN
F 2 "" H 4488 6500 50  0000 C CNN
F 3 "" H 4450 6650 50  0000 C CNN
	1    4450 6650
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58F98E43
P 4850 6650
F 0 "C?" H 4875 6750 50  0000 L CNN
F 1 "0.1uF" H 4875 6550 50  0000 L CNN
F 2 "" H 4888 6500 50  0000 C CNN
F 3 "" H 4850 6650 50  0000 C CNN
	1    4850 6650
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58F98E44
P 5150 6650
F 0 "C?" H 5175 6750 50  0000 L CNN
F 1 "100pF" H 5175 6550 50  0000 L CNN
F 2 "" H 5188 6500 50  0000 C CNN
F 3 "" H 5150 6650 50  0000 C CNN
	1    5150 6650
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58F98E45
P 5550 6650
F 0 "C?" H 5575 6750 50  0000 L CNN
F 1 "0.1uF" H 5600 6550 50  0000 L CNN
F 2 "" H 5588 6500 50  0000 C CNN
F 3 "" H 5550 6650 50  0000 C CNN
	1    5550 6650
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58F98E46
P 5850 6650
F 0 "C?" H 5875 6750 50  0000 L CNN
F 1 "100pF" H 5875 6550 50  0000 L CNN
F 2 "" H 5888 6500 50  0000 C CNN
F 3 "" H 5850 6650 50  0000 C CNN
	1    5850 6650
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58F98E47
P 6250 6650
F 0 "C?" H 6275 6750 50  0000 L CNN
F 1 "0.1uF" H 6275 6550 50  0000 L CNN
F 2 "" H 6288 6500 50  0000 C CNN
F 3 "" H 6250 6650 50  0000 C CNN
	1    6250 6650
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58F98E48
P 6550 6650
F 0 "C?" H 6575 6750 50  0000 L CNN
F 1 "100pF" H 6575 6550 50  0000 L CNN
F 2 "" H 6588 6500 50  0000 C CNN
F 3 "" H 6550 6650 50  0000 C CNN
	1    6550 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 6600 3600 6600
Wire Wire Line
	3200 6600 3200 6550
Connection ~ 3200 6600
Wire Wire Line
	4150 6500 6550 6500
Wire Wire Line
	4150 6800 6550 6800
Connection ~ 4450 6800
Connection ~ 4850 6800
Connection ~ 5150 6800
Connection ~ 5550 6800
Connection ~ 5850 6800
Connection ~ 6250 6800
Connection ~ 6550 6800
$Comp
L VCC #PWR022
U 1 1 58F98E49
P 5350 6450
F 0 "#PWR022" H 5350 6300 50  0001 C CNN
F 1 "VCC" H 5350 6600 50  0000 C CNN
F 2 "" H 5350 6450 50  0000 C CNN
F 3 "" H 5350 6450 50  0000 C CNN
	1    5350 6450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR023
U 1 1 58F98E4A
P 5350 6850
F 0 "#PWR023" H 5350 6600 50  0001 C CNN
F 1 "GND" H 5350 6700 50  0000 C CNN
F 2 "" H 5350 6850 50  0000 C CNN
F 3 "" H 5350 6850 50  0000 C CNN
	1    5350 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 6850 5350 6800
Connection ~ 5350 6800
Wire Wire Line
	5350 6450 5350 6500
Connection ~ 5350 6500
Wire Notes Line
	6800 7100 6800 6200
Wire Notes Line
	6800 6200 2700 6200
Wire Notes Line
	2700 6200 2700 7100
Wire Notes Line
	2700 7100 6800 7100
Wire Wire Line
	3800 6500 3600 6500
Wire Wire Line
	3600 6500 3600 6600
Connection ~ 3500 6600
Wire Wire Line
	3800 6800 3600 6800
Wire Wire Line
	3600 6800 3600 6700
Connection ~ 3500 6700
$Comp
L CP1 C?
U 1 1 58F98E4B
P 3800 6650
F 0 "C?" H 3825 6750 50  0000 L CNN
F 1 "22uF" H 3825 6550 50  0000 L CNN
F 2 "" H 3800 6650 50  0000 C CNN
F 3 "" H 3800 6650 50  0000 C CNN
	1    3800 6650
	1    0    0    -1  
$EndComp
Wire Notes Line
	4800 4300 4800 5600
Wire Notes Line
	4800 5600 8400 5600
Wire Notes Line
	8400 5600 8400 4300
Wire Notes Line
	8400 4300 4800 4300
Text Notes 6050 4250 0    60   ~ 0
Signal Flow and Connections
Text Notes 4250 6150 0    60   ~ 0
Power Input and Bypass Caps
$EndSCHEMATC
