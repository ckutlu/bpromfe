EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:detectors
LIBS:rfamps
LIBS:rfconn
LIBS:rfdiodes
LIBS:rftrans
LIBS:opamps
LIBS:bpromfe-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BAT15-099 D1
U 2 1 58FDFA22
P 1950 4700
F 0 "D1" H 1950 4800 50  0000 C CNN
F 1 "BAT15-099" H 2050 4900 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-143_Handsoldering" H 1950 4700 50  0001 C CNN
F 3 "" H 1950 4700 50  0000 C CNN
	2    1950 4700
	1    0    0    -1  
$EndComp
$Comp
L BAT15-099 D1
U 1 1 58FDFA74
P 3550 4900
F 0 "D1" H 3550 5000 50  0000 C CNN
F 1 "BAT15-099" H 3550 4800 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-143_Handsoldering" H 3550 4900 50  0001 C CNN
F 3 "" H 3550 4900 50  0000 C CNN
	1    3550 4900
	-1   0    0    1   
$EndComp
$Comp
L AD8000YRDZ U8
U 1 1 58FDFB26
P 2650 4850
F 0 "U8" H 2600 5150 50  0000 L CNN
F 1 "AD8000YRDZ" H 2300 4550 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8-1EP_3.9x4.9mm_Pitch1.27mm" H 3000 5250 50  0001 C CNN
F 3 "" H 3000 4600 50  0000 C CNN
	1    2650 4850
	1    0    0    -1  
$EndComp
$Comp
L R RF4
U 1 1 58FDFE3F
P 4750 4700
F 0 "RF4" V 4650 4700 50  0000 C CNN
F 1 "432" V 4750 4700 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4680 4700 50  0001 C CNN
F 3 "" H 4750 4700 50  0000 C CNN
	1    4750 4700
	0    1    1    0   
$EndComp
$Comp
L R RFB2
U 1 1 58FDFFE7
P 4100 4100
F 0 "RFB2" V 4180 4100 50  0000 C CNN
F 1 "2k" V 4100 4100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4030 4100 50  0001 C CNN
F 3 "" H 4100 4100 50  0000 C CNN
	1    4100 4100
	0    1    1    0   
$EndComp
$Comp
L R RFB1
U 1 1 58FE0102
P 3900 4300
F 0 "RFB1" V 3980 4300 50  0000 C CNN
F 1 "2k" V 3900 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3830 4300 50  0001 C CNN
F 3 "" H 3900 4300 50  0000 C CNN
	1    3900 4300
	-1   0    0    1   
$EndComp
$Comp
L R RF3
U 1 1 58FE0385
P 1550 4700
F 0 "RF3" V 1630 4700 50  0000 C CNN
F 1 "427" V 1550 4700 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1480 4700 50  0001 C CNN
F 3 "" H 1550 4700 50  0000 C CNN
	1    1550 4700
	0    -1   -1   0   
$EndComp
$Comp
L R RS4
U 1 1 58FE044F
P 1900 5100
F 0 "RS4" V 1800 5100 50  0000 C CNN
F 1 "50" V 1900 5100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1830 5100 50  0001 C CNN
F 3 "" H 1900 5100 50  0000 C CNN
	1    1900 5100
	1    0    0    -1  
$EndComp
Text Notes 7400 7500 0    60   ~ 0
CFOA Based High Speed Peak Detect and Hold
Text Notes 8200 7650 0    60   ~ 0
24.04.2017
Text Notes 10200 7500 0    60   ~ 0
Author: Caglar Kutlu
$Comp
L VCC #PWR050
U 1 1 58FE369D
P 2300 6200
F 0 "#PWR050" H 2300 6050 50  0001 C CNN
F 1 "VCC" H 2300 6350 50  0000 C CNN
F 2 "" H 2300 6200 50  0000 C CNN
F 3 "" H 2300 6200 50  0000 C CNN
	1    2300 6200
	1    0    0    -1  
$EndComp
$Comp
L C CHOLD1
U 1 1 58FE3CF9
P 4300 5250
F 0 "CHOLD1" H 4325 5350 50  0000 L CNN
F 1 "C" H 4325 5150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4338 5100 50  0001 C CNN
F 3 "" H 4300 5250 50  0000 C CNN
	1    4300 5250
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR051
U 1 1 58FE3ED2
P 3250 4750
F 0 "#PWR051" H 3250 4600 50  0001 C CNN
F 1 "VCC" H 3250 4900 50  0000 C CNN
F 2 "" H 3250 4750 50  0000 C CNN
F 3 "" H 3250 4750 50  0000 C CNN
	1    3250 4750
	1    0    0    -1  
$EndComp
$Comp
L VEE #PWR052
U 1 1 58FE440B
P 2150 5000
F 0 "#PWR052" H 2150 4850 50  0001 C CNN
F 1 "VEE" H 2150 5150 50  0000 C CNN
F 2 "" H 2150 5000 50  0000 C CNN
F 3 "" H 2150 5000 50  0000 C CNN
	1    2150 5000
	-1   0    0    1   
$EndComp
$Comp
L AD8000YRDZ U9
U 1 1 58FE4DD4
P 5500 4850
F 0 "U9" H 5450 5150 50  0000 L CNN
F 1 "AD8000YRDZ" H 5150 4550 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8-1EP_3.9x4.9mm_Pitch1.27mm" H 5850 5250 50  0001 C CNN
F 3 "" H 5850 4600 50  0000 C CNN
	1    5500 4850
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR053
U 1 1 58FE4DDA
P 6100 4700
F 0 "#PWR053" H 6100 4550 50  0001 C CNN
F 1 "VCC" H 6100 4850 50  0000 C CNN
F 2 "" H 6100 4700 50  0000 C CNN
F 3 "" H 6100 4700 50  0000 C CNN
	1    6100 4700
	1    0    0    -1  
$EndComp
$Comp
L VEE #PWR054
U 1 1 58FE4DE1
P 5000 5000
F 0 "#PWR054" H 5000 4850 50  0001 C CNN
F 1 "VEE" H 5000 5150 50  0000 C CNN
F 2 "" H 5000 5000 50  0000 C CNN
F 3 "" H 5000 5000 50  0000 C CNN
	1    5000 5000
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 58FE5632
P 4000 4900
F 0 "R2" V 4080 4900 50  0000 C CNN
F 1 "50" V 4000 4900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3930 4900 50  0001 C CNN
F 3 "" H 4000 4900 50  0000 C CNN
	1    4000 4900
	0    1    1    0   
$EndComp
$Comp
L GND #PWR055
U 1 1 58FE5731
P 4300 5600
F 0 "#PWR055" H 4300 5350 50  0001 C CNN
F 1 "GND" H 4300 5450 50  0000 C CNN
F 2 "" H 4300 5600 50  0000 C CNN
F 3 "" H 4300 5600 50  0000 C CNN
	1    4300 5600
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 58FE59C0
P 4700 4900
F 0 "R3" V 4780 4900 50  0000 C CNN
F 1 "50" V 4700 4900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4630 4900 50  0001 C CNN
F 3 "" H 4700 4900 50  0000 C CNN
	1    4700 4900
	0    1    1    0   
$EndComp
Text Notes 7150 6250 0    60   ~ 0
NOTES\n=====\n- The series resistor right in front of the non-inverting pins of the amplifiers are \nfor decoupling the input capacitance of them. Which means, they help to reduce \nthe peaking if there is any. It can be selected around 50 - 150 Ohm, or \nomitted altogether if no peaking is observed.\n\n- DISCHG pin opens the activates the JFET at TTL Low discharging the capacitor, \ndeactivates it at TTL High letting the capacitor to fill.  P.S. : Actual low and high \nvalues should be determined with the particular jfet used.\n\n- The resistors in the Feedback Network are selected to have a unity overall gain\nin the 200 MHz band via spice simulations. However, they should be varied if\nany stability issues occur. An open relation relating the value of the resistors and\nthe closed loop gain of the circuit is due.\n\n- CFOA_DET_IND is assumed to be 50 Ohm terminated
Text HLabel 6300 4900 2    60   Output ~ 0
CFOA_DET_OUT
Text HLabel 3300 5700 0    60   Input ~ 0
~DSCHG~
$Comp
L GND #PWR056
U 1 1 58FE8CBA
P 3900 4500
F 0 "#PWR056" H 3900 4250 50  0001 C CNN
F 1 "GND" H 3900 4350 50  0000 C CNN
F 2 "" H 3900 4500 50  0000 C CNN
F 3 "" H 3900 4500 50  0000 C CNN
	1    3900 4500
	1    0    0    -1  
$EndComp
Text HLabel 1650 4900 0    60   Input ~ 0
CFOA_DET_IN
Text Notes 3650 3950 0    60   ~ 0
Feedback Network
$Comp
L GND #PWR057
U 1 1 58FEC382
P 1900 5350
F 0 "#PWR057" H 1900 5100 50  0001 C CNN
F 1 "GND" H 1900 5200 50  0000 C CNN
F 2 "" H 1900 5350 50  0000 C CNN
F 3 "" H 1900 5350 50  0000 C CNN
	1    1900 5350
	1    0    0    -1  
$EndComp
$Comp
L C CBH11
U 1 1 58FF841D
P 1800 6600
F 0 "CBH11" H 1825 6700 50  0000 L CNN
F 1 "0.1u" H 1825 6500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1838 6450 50  0001 C CNN
F 3 "" H 1800 6600 50  0000 C CNN
	1    1800 6600
	1    0    0    -1  
$EndComp
$Comp
L C CBL11
U 1 1 58FF8494
P 2050 6600
F 0 "CBL11" H 2075 6700 50  0000 L CNN
F 1 "100p" H 2075 6500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2088 6450 50  0001 C CNN
F 3 "" H 2050 6600 50  0000 C CNN
	1    2050 6600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR058
U 1 1 58FF8E70
P 3000 6900
F 0 "#PWR058" H 3000 6650 50  0001 C CNN
F 1 "GND" H 3000 6750 50  0000 C CNN
F 2 "" H 3000 6900 50  0000 C CNN
F 3 "" H 3000 6900 50  0000 C CNN
	1    3000 6900
	1    0    0    -1  
$EndComp
$Comp
L C CBH13
U 1 1 58FF8F4C
P 2450 6600
F 0 "CBH13" H 2475 6700 50  0000 L CNN
F 1 "0.1u" H 2475 6500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2488 6450 50  0001 C CNN
F 3 "" H 2450 6600 50  0000 C CNN
	1    2450 6600
	1    0    0    -1  
$EndComp
$Comp
L C CBL13
U 1 1 58FF8F52
P 2700 6600
F 0 "CBL13" H 2725 6700 50  0000 L CNN
F 1 "100p" H 2725 6500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2738 6450 50  0001 C CNN
F 3 "" H 2700 6600 50  0000 C CNN
	1    2700 6600
	1    0    0    -1  
$EndComp
$Comp
L C CBH12
U 1 1 58FF8FFE
P 1800 7100
F 0 "CBH12" H 1825 7200 50  0000 L CNN
F 1 "0.1u" H 1825 7000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1838 6950 50  0001 C CNN
F 3 "" H 1800 7100 50  0000 C CNN
	1    1800 7100
	1    0    0    -1  
$EndComp
$Comp
L C CBL12
U 1 1 58FF9004
P 2050 7100
F 0 "CBL12" H 2075 7200 50  0000 L CNN
F 1 "100p" H 2075 7000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2088 6950 50  0001 C CNN
F 3 "" H 2050 7100 50  0000 C CNN
	1    2050 7100
	1    0    0    -1  
$EndComp
$Comp
L C CBH14
U 1 1 58FF90B0
P 2450 7100
F 0 "CBH14" H 2475 7200 50  0000 L CNN
F 1 "0.1u" H 2475 7000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2488 6950 50  0001 C CNN
F 3 "" H 2450 7100 50  0000 C CNN
	1    2450 7100
	1    0    0    -1  
$EndComp
$Comp
L C CBL14
U 1 1 58FF90B6
P 2700 7100
F 0 "CBL14" H 2725 7200 50  0000 L CNN
F 1 "100p" H 2725 7000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2738 6950 50  0001 C CNN
F 3 "" H 2700 7100 50  0000 C CNN
	1    2700 7100
	1    0    0    -1  
$EndComp
$Comp
L VEE #PWR059
U 1 1 58FF9715
P 2250 7400
F 0 "#PWR059" H 2250 7250 50  0001 C CNN
F 1 "VEE" H 2250 7550 50  0000 C CNN
F 2 "" H 2250 7400 50  0000 C CNN
F 3 "" H 2250 7400 50  0000 C CNN
	1    2250 7400
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR060
U 1 1 5901F562
P 5700 5250
F 0 "#PWR060" H 5700 5000 50  0001 C CNN
F 1 "GND" H 5700 5100 50  0000 C CNN
F 2 "" H 5700 5250 50  0000 C CNN
F 3 "" H 5700 5250 50  0000 C CNN
	1    5700 5250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR061
U 1 1 5901F77A
P 2850 5250
F 0 "#PWR061" H 2850 5000 50  0001 C CNN
F 1 "GND" H 2850 5100 50  0000 C CNN
F 2 "" H 2850 5250 50  0000 C CNN
F 3 "" H 2850 5250 50  0000 C CNN
	1    2850 5250
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 59054608
P 3400 5500
F 0 "R4" V 3480 5500 50  0000 C CNN
F 1 "R" V 3400 5500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3330 5500 50  0001 C CNN
F 3 "" H 3400 5500 50  0000 C CNN
	1    3400 5500
	-1   0    0    1   
$EndComp
$Comp
L R R5
U 1 1 59054D9E
P 3600 5500
F 0 "R5" V 3680 5500 50  0000 C CNN
F 1 "R" V 3600 5500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3530 5500 50  0001 C CNN
F 3 "" H 3600 5500 50  0000 C CNN
	1    3600 5500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR062
U 1 1 59054EAA
P 3600 5700
F 0 "#PWR062" H 3600 5450 50  0001 C CNN
F 1 "GND" H 3600 5550 50  0000 C CNN
F 2 "" H 3600 5700 50  0000 C CNN
F 3 "" H 3600 5700 50  0000 C CNN
	1    3600 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5700 3600 5650
Connection ~ 3600 5300
Wire Wire Line
	3600 5350 3600 5300
Wire Wire Line
	3400 5700 3400 5650
Wire Wire Line
	3300 5700 3400 5700
Wire Wire Line
	3400 5350 3400 5300
Wire Wire Line
	3400 5300 3650 5300
Wire Wire Line
	2850 5200 2850 5250
Wire Wire Line
	5700 5200 5700 5250
Connection ~ 2450 7300
Connection ~ 2250 7300
Wire Wire Line
	2250 7300 2250 7400
Connection ~ 2050 7300
Connection ~ 2700 6850
Connection ~ 2450 6850
Connection ~ 1800 6850
Connection ~ 2050 6850
Wire Wire Line
	1200 6850 3000 6850
Connection ~ 2300 6400
Connection ~ 2050 6400
Connection ~ 2450 6400
Wire Wire Line
	2300 6400 2300 6200
Wire Wire Line
	2450 7300 2450 7250
Wire Wire Line
	2700 7300 2700 7250
Wire Wire Line
	1800 7300 1800 7250
Wire Wire Line
	1200 7300 2700 7300
Wire Wire Line
	2050 7250 2050 7300
Wire Wire Line
	2450 6750 2450 6950
Wire Wire Line
	2700 6750 2700 6950
Wire Wire Line
	2700 6400 2700 6450
Wire Wire Line
	2450 6400 2450 6450
Wire Wire Line
	1800 6750 1800 6950
Wire Wire Line
	2050 6750 2050 6950
Wire Wire Line
	2050 6400 2050 6450
Wire Wire Line
	1800 6400 1800 6450
Wire Wire Line
	1200 6400 2700 6400
Wire Wire Line
	1900 5250 1900 5350
Connection ~ 1900 4900
Wire Wire Line
	1900 4950 1900 4900
Wire Notes Line
	4400 4000 3650 4000
Wire Notes Line
	4400 4700 4400 4000
Wire Notes Line
	3650 4700 4400 4700
Wire Notes Line
	3650 4000 3650 4700
Wire Wire Line
	1650 4900 2200 4900
Connection ~ 3900 4100
Connection ~ 1350 4700
Wire Wire Line
	1350 4800 2200 4800
Wire Wire Line
	1350 4100 1350 4800
Wire Wire Line
	1400 4700 1350 4700
Wire Wire Line
	1800 4700 1700 4700
Wire Wire Line
	3900 4100 3900 4150
Wire Wire Line
	1350 4100 3950 4100
Wire Wire Line
	3900 4450 3900 4500
Connection ~ 4300 5550
Wire Wire Line
	3950 5550 4300 5550
Wire Wire Line
	3950 5500 3950 5550
Connection ~ 4300 5050
Wire Wire Line
	3950 5050 4300 5050
Wire Wire Line
	3950 5100 3950 5050
Wire Wire Line
	4900 4700 5050 4700
Wire Wire Line
	4600 4800 5050 4800
Wire Wire Line
	4600 4700 4600 4800
Connection ~ 4300 4900
Wire Wire Line
	4850 4900 5050 4900
Wire Wire Line
	3100 4900 3400 4900
Wire Wire Line
	3700 4900 3850 4900
Wire Wire Line
	4150 4900 4550 4900
Wire Wire Line
	4300 4900 4300 5100
Wire Wire Line
	4300 5400 4300 5600
Wire Wire Line
	5000 5000 5050 5000
Wire Wire Line
	5950 4800 6100 4800
Wire Wire Line
	3100 4800 3250 4800
Wire Wire Line
	2150 5000 2200 5000
Text Notes 3450 6500 0    60   ~ 0
- R4 and R5 might be necessary for reducing\n peaks depending on the situation.\n- If you don't wanna use them. \nPlace 0 Ohm at R4 and don't connect R5
Wire Wire Line
	2100 4700 2100 4350
Wire Wire Line
	2100 4350 3300 4350
Wire Wire Line
	3300 4350 3300 4900
Connection ~ 3300 4900
NoConn ~ 2200 4700
Text Notes 2100 4350 0    60   ~ 0
FOR THE EASE OF LAYOUT
Text Notes 3950 2850 0    60   ~ 0
BA15-099 Package is antiparallel diodes\nI was planning to connect them differently that's why I chose that package.\nBut the plans changed and I already got them, and this package causes some \nproblems in the layout. If you plan to improve, select a different matched diode\n package accordingly.
$Comp
L Q_PJFET_DSG Q1
U 1 1 590503C6
P 3850 5300
F 0 "Q1" H 4050 5350 50  0000 L CNN
F 1 "Q_DIS" H 4050 5250 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23_Handsoldering" H 4050 5400 50  0001 C CNN
F 3 "" H 3850 5300 50  0000 C CNN
	1    3850 5300
	1    0    0    1   
$EndComp
Wire Wire Line
	5950 4900 6300 4900
Wire Wire Line
	5000 4700 5000 4100
Wire Wire Line
	5000 4100 4250 4100
Connection ~ 5000 4700
Wire Wire Line
	3250 4800 3250 4750
Wire Wire Line
	3100 4500 3100 4700
Wire Wire Line
	6100 4800 6100 4700
Wire Wire Line
	5750 4400 6000 4400
Wire Wire Line
	6000 4400 6000 4700
Wire Wire Line
	6000 4700 5950 4700
Text HLabel 5750 4400 0    60   Input ~ 0
~PDWN~
Text HLabel 3100 4500 0    60   Input ~ 0
~PDWN~
Wire Wire Line
	3000 6850 3000 6900
$Comp
L CP1 CR19
U 1 1 5906B349
P 1500 6600
F 0 "CR19" H 1525 6700 50  0000 L CNN
F 1 "22u" H 1525 6500 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 1500 6600 50  0001 C CNN
F 3 "" H 1500 6600 50  0000 C CNN
	1    1500 6600
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR20
U 1 1 5906B4B0
P 1500 7100
F 0 "CR20" H 1525 7200 50  0000 L CNN
F 1 "22u" H 1525 7000 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 1500 7100 50  0001 C CNN
F 3 "" H 1500 7100 50  0000 C CNN
	1    1500 7100
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR17
U 1 1 5906B59E
P 1200 6600
F 0 "CR17" H 1225 6700 50  0000 L CNN
F 1 "22u" H 1225 6500 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 1200 6600 50  0001 C CNN
F 3 "" H 1200 6600 50  0000 C CNN
	1    1200 6600
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR18
U 1 1 5906B606
P 1200 7100
F 0 "CR18" H 1225 7200 50  0000 L CNN
F 1 "22u" H 1225 7000 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 1200 7100 50  0001 C CNN
F 3 "" H 1200 7100 50  0000 C CNN
	1    1200 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 6950 1200 6750
Wire Wire Line
	1500 6750 1500 6950
Connection ~ 1200 6850
Connection ~ 1500 6850
Wire Wire Line
	1500 7250 1500 7300
Connection ~ 1800 7300
Wire Wire Line
	1200 7250 1200 7300
Connection ~ 1500 7300
Wire Wire Line
	1200 6450 1200 6400
Connection ~ 1800 6400
Wire Wire Line
	1500 6450 1500 6400
Connection ~ 1500 6400
$EndSCHEMATC
