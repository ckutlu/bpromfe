EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:detectors
LIBS:rfamps
LIBS:rfconn
LIBS:rfdiodes
LIBS:rftrans
LIBS:opamps
LIBS:bpromfe-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BGA427 U2
U 1 1 58F98E15
P 4950 1250
F 0 "U2" H 4950 1400 50  0000 L CNN
F 1 "BGA427" H 4950 1100 50  0000 L CNN
F 2 "footprints:SOT-343_CUSTOM_Handsoldering" H 4850 1300 50  0001 C CNN
F 3 "" H 4950 1400 50  0000 C CNN
	1    4950 1250
	1    0    0    -1  
$EndComp
$Comp
L BGA2851 U3
U 1 1 58F98E16
P 7350 1250
F 0 "U3" H 7350 1400 50  0000 L CNN
F 1 "BGA2851" H 7350 1100 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6_Handsoldering" H 7250 1300 50  0001 C CNN
F 3 "" H 7350 1400 50  0000 C CNN
	1    7350 1250
	1    0    0    -1  
$EndComp
$Comp
L BGA2869 U7
U 1 1 58F98E17
P 10200 1250
F 0 "U7" H 10200 1400 50  0000 L CNN
F 1 "BGA2869" H 10200 1100 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6_Handsoldering" H 10100 1300 50  0001 C CNN
F 3 "" H 10200 1400 50  0000 C CNN
	1    10200 1250
	1    0    0    -1  
$EndComp
$Comp
L LTC5507 U4
U 1 1 58F98E18
P 7950 2950
F 0 "U4" H 7950 2700 60  0000 C CNN
F 1 "LTC5507" H 7950 3150 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:TSOT-23-6_MK06A_HandSoldering" H 7750 2950 60  0001 C CNN
F 3 "" H 7750 2950 60  0001 C CNN
	1    7950 2950
	-1   0    0    1   
$EndComp
$Comp
L C CD1
U 1 1 58F98E19
P 1550 1250
F 0 "CD1" V 1700 1200 50  0000 L CNN
F 1 "470 pF" V 1400 1100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1588 1100 50  0001 C CNN
F 3 "" H 1550 1250 50  0000 C CNN
	1    1550 1250
	0    -1   -1   0   
$EndComp
$Comp
L C C2
U 1 1 58F98E1A
P 1900 1450
F 0 "C2" H 1925 1550 50  0000 L CNN
F 1 "7.958 pF" H 1925 1350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1938 1300 50  0001 C CNN
F 3 "" H 1900 1450 50  0000 C CNN
	1    1900 1450
	1    0    0    -1  
$EndComp
Text Notes 5100 1950 0    60   ~ 0
Power Gains of Amps ( Typical Values )\n-----------------------\nBGA427   :    27 dB\nBGA2851 :    23.2 dB\nBGA2869 :    31.1 dB
$Comp
L VCC #PWR25
U 1 1 58F98E1B
P 4850 900
F 0 "#PWR25" H 4850 750 50  0001 C CNN
F 1 "VCC" H 4850 1050 50  0000 C CNN
F 2 "" H 4850 900 50  0000 C CNN
F 3 "" H 4850 900 50  0000 C CNN
	1    4850 900 
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR31
U 1 1 58F98E1C
P 7250 950
F 0 "#PWR31" H 7250 800 50  0001 C CNN
F 1 "VCC" H 7250 1100 50  0000 C CNN
F 2 "" H 7250 950 50  0000 C CNN
F 3 "" H 7250 950 50  0000 C CNN
	1    7250 950 
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR45
U 1 1 58F98E1D
P 10100 950
F 0 "#PWR45" H 10100 800 50  0001 C CNN
F 1 "VCC" H 10100 1100 50  0000 C CNN
F 2 "" H 10100 950 50  0000 C CNN
F 3 "" H 10100 950 50  0000 C CNN
	1    10100 950 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR26
U 1 1 58F98E1E
P 4850 1550
F 0 "#PWR26" H 4850 1300 50  0001 C CNN
F 1 "GND" H 4850 1400 50  0000 C CNN
F 2 "" H 4850 1550 50  0000 C CNN
F 3 "" H 4850 1550 50  0000 C CNN
	1    4850 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR32
U 1 1 58F98E1F
P 7250 1550
F 0 "#PWR32" H 7250 1300 50  0001 C CNN
F 1 "GND" H 7250 1400 50  0000 C CNN
F 2 "" H 7250 1550 50  0000 C CNN
F 3 "" H 7250 1550 50  0000 C CNN
	1    7250 1550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR46
U 1 1 58F98E20
P 10100 1550
F 0 "#PWR46" H 10100 1300 50  0001 C CNN
F 1 "GND" H 10100 1400 50  0000 C CNN
F 2 "" H 10100 1550 50  0000 C CNN
F 3 "" H 10100 1550 50  0000 C CNN
	1    10100 1550
	1    0    0    -1  
$EndComp
Text Notes 5500 600  0    60   ~ 0
AMPLIFIER BLOCK
$Comp
L C C4
U 1 1 58F98E21
P 2600 1450
F 0 "C4" H 2625 1550 50  0000 L CNN
F 1 "7.958 pF" H 2625 1350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2638 1300 50  0001 C CNN
F 3 "" H 2600 1450 50  0000 C CNN
	1    2600 1450
	1    0    0    -1  
$EndComp
$Comp
L L L2
U 1 1 58F98E22
P 2250 1250
F 0 "L2" V 2200 1250 50  0000 C CNN
F 1 "39.79nH" V 2325 1250 50  0000 C CNN
F 2 "footprints:L_0603_HandSoldering" H 2250 1250 50  0001 C CNN
F 3 "" H 2250 1250 50  0000 C CNN
	1    2250 1250
	0    1    1    0   
$EndComp
$Comp
L GND #PWR14
U 1 1 58F98E23
P 1900 1600
F 0 "#PWR14" H 1900 1350 50  0001 C CNN
F 1 "GND" H 1900 1450 50  0000 C CNN
F 2 "" H 1900 1600 50  0000 C CNN
F 3 "" H 1900 1600 50  0000 C CNN
	1    1900 1600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR16
U 1 1 58F98E24
P 2600 1600
F 0 "#PWR16" H 2600 1350 50  0001 C CNN
F 1 "GND" H 2600 1450 50  0000 C CNN
F 2 "" H 2600 1600 50  0000 C CNN
F 3 "" H 2600 1600 50  0000 C CNN
	1    2600 1600
	1    0    0    -1  
$EndComp
Text Notes 1600 950  0    60   ~ 0
Preselection Filter
Text Notes 900  2050 0    60   ~ 0
 (s-parameters @ qucs "bpromfe_input_filter.sch")
Text Notes 6750 5950 0    60   Italic 0
Notes On Amplifier Block\n\n========================\n- Make sure that 0 Ohm resistors are compatible with capacitor footprints.\n- If you want to bypass BGA2869 :\n    --> Don't solder CB1, solder 0 Ohm to RB1 and RB2, solder CB2\n- RBx are 0 Ohm (smt jumper)\n- All CDx can also be 0 Ohm components depending on the experimentation\nmeasurements. Otherwise, they should be chosen as DC blocking (high-pass)\ncapacitors for passing above approx. 1 MHz. 100 -560 pF is a good range. \nPick 100 pF if in doubt. That corresponds to a 20 MHz cut-off for the 1st order\nRC filter formed with the input resistance of the amplifier in front.\n- One can go for a second order low pass in front of BGA2851. Which would\nintroduce an inductor for maybe not so much reward in return. \n
$Comp
L R RB3
U 1 1 58F98E25
P 6600 1450
F 0 "RB3" V 6680 1450 50  0000 C CNN
F 1 "0" H 6500 1550 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6530 1450 50  0001 C CNN
F 3 "" H 6600 1450 50  0000 C CNN
	1    6600 1450
	-1   0    0    1   
$EndComp
$Comp
L C CD4
U 1 1 58F98E26
P 6850 1250
F 0 "CD4" V 7000 1250 50  0000 L CNN
F 1 "100 pF" V 6700 1100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6888 1100 50  0001 C CNN
F 3 "" H 6850 1250 50  0000 C CNN
	1    6850 1250
	0    -1   -1   0   
$EndComp
$Comp
L R RB4
U 1 1 58F98E27
P 7800 1500
F 0 "RB4" V 7880 1500 50  0000 C CNN
F 1 "0" H 7700 1600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7730 1500 50  0001 C CNN
F 3 "" H 7800 1500 50  0000 C CNN
	1    7800 1500
	-1   0    0    1   
$EndComp
$Comp
L C CD6
U 1 1 58F98E28
P 9700 1250
F 0 "CD6" V 9850 1200 50  0000 L CNN
F 1 "100 pF" V 9550 1100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 9738 1100 50  0001 C CNN
F 3 "" H 9700 1250 50  0000 C CNN
	1    9700 1250
	0    -1   -1   0   
$EndComp
Text Label 7050 1850 0    60   Italic 0
AMPBLK_BYP1
$Comp
L C C5
U 1 1 58F98E29
P 4000 3100
F 0 "C5" H 4025 3200 50  0000 L CNN
F 1 "19 pF" H 4025 3000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4038 2950 50  0001 C CNN
F 3 "" H 4000 3100 50  0000 C CNN
	1    4000 3100
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 58F98E2A
P 4700 3100
F 0 "C6" H 4725 3200 50  0000 L CNN
F 1 "19 pF" H 4725 3000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4738 2950 50  0001 C CNN
F 3 "" H 4700 3100 50  0000 C CNN
	1    4700 3100
	1    0    0    -1  
$EndComp
$Comp
L L L3
U 1 1 58F98E2B
P 4350 2900
F 0 "L3" V 4300 2900 50  0000 C CNN
F 1 "52.84 nH" V 4425 2900 50  0000 C CNN
F 2 "footprints:L_0603_HandSoldering" H 4350 2900 50  0001 C CNN
F 3 "" H 4350 2900 50  0000 C CNN
	1    4350 2900
	0    1    1    0   
$EndComp
$Comp
L GND #PWR20
U 1 1 58F98E2C
P 4000 3250
F 0 "#PWR20" H 4000 3000 50  0001 C CNN
F 1 "GND" H 4000 3100 50  0000 C CNN
F 2 "" H 4000 3250 50  0000 C CNN
F 3 "" H 4000 3250 50  0000 C CNN
	1    4000 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR22
U 1 1 58F98E2D
P 4700 3250
F 0 "#PWR22" H 4700 3000 50  0001 C CNN
F 1 "GND" H 4700 3100 50  0000 C CNN
F 2 "" H 4700 3250 50  0000 C CNN
F 3 "" H 4700 3250 50  0000 C CNN
	1    4700 3250
	1    0    0    -1  
$EndComp
Text Notes 4000 2600 0    60   ~ 0
Acquisition Filter 2
Text Notes 3100 3700 0    60   ~ 0
 (s-parameters @ qucs "bpromfe_acquisition_filter.sch")
Text Notes 12000 4000 0    60   Italic 0
NOTE ON LTC5507 INPUT MATCHING\n==========================\n- The RF_IN port of the LTC5507 is 250 Ohm. Simple 60 Ohm resistor would\nmake an 'okay' match to prevent reflections. S21 without resistor is around \n-3 dB, with resistor -7 dB. |S21|^2=> -14 dB. Considering we match, this adds\n NF_4 = 14 dB. Calculating total NF with 3 gain and 1 attenuation due to \nmismatch stage (nfOfChain.m). NF only degrades 0.015.  Of course we lose 14 dB \nin power.\n\n- If wanted the matching resistor could be left unconnected, corresponding to\nmore reflected power (S11 = -5 dB) if the previous stage could handle that.\n\n- An ideal case would be to use a buffer amp, but I doubt that's necessary now.
$Comp
L C CD2
U 1 1 58F98E2E
P 3400 6200
F 0 "CD2" V 3250 6150 50  0000 L CNN
F 1 "1.8 nF" V 3550 6150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3438 6050 50  0001 C CNN
F 3 "" H 3400 6200 50  0000 C CNN
	1    3400 6200
	0    1    1    0   
$EndComp
Text Notes 6350 4250 0    60   Italic 0
NOTES ON LTC5507\n============\n- PCAP selected to have corner freq. at 20 MHz (see datasheet). \n- See input matching notes at the corner\n- The output bandwidth should be around 150 kHz.\n- According to datasheet there is a 250 mV typ. DC offset \nat the LTC output.\n- The output source current is rated as 3 mA. This is definitely insufficient for\ndriving coax. That's why there is an opamp at the output.
$Comp
L C_Small CLP1
U 1 1 58F98E2F
P 7350 2850
F 0 "CLP1" V 7250 2800 50  0000 L CNN
F 1 "1.8nF" V 7300 2550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 7350 2850 50  0001 C CNN
F 3 "" H 7350 2850 50  0000 C CNN
	1    7350 2850
	0    1    1    0   
$EndComp
$Comp
L GND #PWR37
U 1 1 58F98E30
P 8600 3000
F 0 "#PWR37" H 8600 2750 50  0001 C CNN
F 1 "GND" H 8600 2850 50  0000 C CNN
F 2 "" H 8600 3000 50  0000 C CNN
F 3 "" H 8600 3000 50  0000 C CNN
	1    8600 3000
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 58F98E31
P 8250 3400
F 0 "R1" V 8330 3400 50  0000 C CNN
F 1 "22k" V 8250 3400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8180 3400 50  0001 C CNN
F 3 "" H 8250 3400 50  0000 C CNN
	1    8250 3400
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR36
U 1 1 58F98E32
P 8100 3400
F 0 "#PWR36" H 8100 3250 50  0001 C CNN
F 1 "VCC" H 8100 3550 50  0000 C CNN
F 2 "" H 8100 3400 50  0000 C CNN
F 3 "" H 8100 3400 50  0000 C CNN
	1    8100 3400
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR34
U 1 1 58F98E34
P 7500 2650
F 0 "#PWR34" H 7500 2500 50  0001 C CNN
F 1 "VCC" H 7500 2800 50  0000 C CNN
F 2 "" H 7500 2650 50  0000 C CNN
F 3 "" H 7500 2650 50  0000 C CNN
	1    7500 2650
	1    0    0    -1  
$EndComp
Text Notes 8200 2200 0    60   Italic 0
PEAK DETECTOR 1
Text Notes 700  1800 0    60   Italic 0
400 MHz LPF Butter\nand DC Block
Text Notes 3250 3350 0    60   Italic 0
240 MHz \n0.1 dB ripple \nLPF Chebyshev
$Comp
L R RLM1
U 1 1 58F98E35
P 7450 3250
F 0 "RLM1" V 7530 3250 50  0000 C CNN
F 1 "60" V 7450 3250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7380 3250 50  0001 C CNN
F 3 "" H 7450 3250 50  0000 C CNN
	1    7450 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR33
U 1 1 58F98E36
P 7450 3400
F 0 "#PWR33" H 7450 3150 50  0001 C CNN
F 1 "GND" H 7450 3250 50  0000 C CNN
F 2 "" H 7450 3400 50  0000 C CNN
F 3 "" H 7450 3400 50  0000 C CNN
	1    7450 3400
	1    0    0    -1  
$EndComp
Text Notes 7150 6750 0    60   ~ 0
This is the schematic of a single detection block.
Text Notes 7400 7500 0    60   ~ 0
20-240 MHz Amplifier-Detector
Text Notes 8150 7650 0    60   ~ 0
04.20.2017
Text Notes 10600 7650 0    60   ~ 0
1
$Comp
L VCC #PWR23
U 1 1 58F98E49
P 4700 7100
F 0 "#PWR23" H 4700 6950 50  0001 C CNN
F 1 "VCC" H 4700 7250 50  0000 C CNN
F 2 "" H 4700 7100 50  0000 C CNN
F 3 "" H 4700 7100 50  0000 C CNN
	1    4700 7100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR24
U 1 1 58F98E4A
P 4700 7500
F 0 "#PWR24" H 4700 7250 50  0001 C CNN
F 1 "GND" H 4700 7350 50  0000 C CNN
F 2 "" H 4700 7500 50  0000 C CNN
F 3 "" H 4700 7500 50  0000 C CNN
	1    4700 7500
	1    0    0    -1  
$EndComp
Text Notes 2250 5550 0    60   ~ 0
Signal Flow and Connections
Text Notes 2250 6800 0    60   ~ 0
Bypass Cap Pairs\n
Text Label 4000 1250 0    60   ~ 0
AMPBLK_IN
Text Label 4100 5750 2    60   ~ 0
AMPBLK_IN
Text Label 11100 1250 2    60   ~ 0
AMPBLK_OUT
Text Label 700  1250 0    60   ~ 0
PRE_FILT_IN
Text Label 3550 1250 2    60   ~ 0
PRE_FILT_OUT
Text Label 3300 2900 0    60   ~ 0
ACQ_FILT2_IN
Text Label 5500 2900 2    60   ~ 0
ACQ_FILT2_OUT
Text Label 6400 3050 0    60   Italic 0
DET_LTC_IN
Text Label 1800 5750 2    60   ~ 0
PRE_FILT_IN
Text Label 1900 5750 0    60   ~ 0
PRE_FILT_OUT
Text Label 4200 5750 0    60   ~ 0
AMPBLK_OUT
Text Label 4300 6200 0    60   ~ 0
ACQ_FILT2_IN
Text Label 2350 6050 0    60   Italic 0
DET_LTC_IN
Text Label 2250 6050 2    60   Italic 0
DET_LTC_OUT
Text HLabel 1150 5750 0    60   Input ~ 0
RF_IN
Text Label 10950 3000 2    60   Italic 0
DET_LTC_OUT
Text Notes 10200 7500 0    60   ~ 0
Author: Çağlar Kutlu
$Comp
L BGA2851 U5
U 1 1 58FDD738
P 8900 1250
F 0 "U5" H 8900 1400 50  0000 L CNN
F 1 "BGA2851" H 8900 1100 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6_Handsoldering" H 8800 1300 50  0001 C CNN
F 3 "" H 8900 1400 50  0000 C CNN
	1    8900 1250
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR38
U 1 1 58FDD73E
P 8800 950
F 0 "#PWR38" H 8800 800 50  0001 C CNN
F 1 "VCC" H 8800 1100 50  0000 C CNN
F 2 "" H 8800 950 50  0000 C CNN
F 3 "" H 8800 950 50  0000 C CNN
	1    8800 950 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR39
U 1 1 58FDD744
P 8800 1550
F 0 "#PWR39" H 8800 1300 50  0001 C CNN
F 1 "GND" H 8800 1400 50  0000 C CNN
F 2 "" H 8800 1550 50  0000 C CNN
F 3 "" H 8800 1550 50  0000 C CNN
	1    8800 1550
	1    0    0    -1  
$EndComp
$Comp
L R RB5
U 1 1 58FDD74A
P 8200 1500
F 0 "RB5" V 8280 1500 50  0000 C CNN
F 1 "0" H 8100 1600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8130 1500 50  0001 C CNN
F 3 "" H 8200 1500 50  0000 C CNN
	1    8200 1500
	-1   0    0    1   
$EndComp
$Comp
L C CD5
U 1 1 58FDD750
P 8400 1250
F 0 "CD5" V 8550 1150 50  0000 L CNN
F 1 "100 pF" V 8250 1100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 8438 1100 50  0001 C CNN
F 3 "" H 8400 1250 50  0000 C CNN
	1    8400 1250
	0    -1   -1   0   
$EndComp
$Comp
L R RB6
U 1 1 58FDD756
P 9400 1500
F 0 "RB6" V 9480 1500 50  0000 C CNN
F 1 "0" H 9300 1600 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9330 1500 50  0001 C CNN
F 3 "" H 9400 1500 50  0000 C CNN
	1    9400 1500
	-1   0    0    1   
$EndComp
Text Label 8600 1850 0    60   Italic 0
AMPBLK_BYP2
$Sheet
S 1250 4450 1500 600 
U 58FDF9D9
F0 "CFOA_PEAK_DETECTOR" 60
F1 "cfoa_peak_detector.sch" 60
F2 "CFOA_DET_OUT" O R 2750 4850 60 
F3 "~DSCHG~" I L 1250 4650 60 
F4 "CFOA_DET_IN" I L 1250 4850 60 
F5 "~PDWN~" I R 2750 4650 60 
$EndSheet
$Comp
L C CBM1
U 1 1 58FF074F
P 5100 900
F 0 "CBM1" H 5125 1000 50  0000 L CNN
F 1 "1n" H 5125 800 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 5138 750 50  0001 C CNN
F 3 "" H 5100 900 50  0000 C CNN
	1    5100 900 
	0    1    1    0   
$EndComp
$Comp
L GND #PWR27
U 1 1 58FF07FC
P 5250 1000
F 0 "#PWR27" H 5250 750 50  0001 C CNN
F 1 "GND" H 5250 850 50  0000 C CNN
F 2 "" H 5250 1000 50  0000 C CNN
F 3 "" H 5250 1000 50  0000 C CNN
	1    5250 1000
	1    0    0    -1  
$EndComp
Text HLabel 1250 4650 0    60   Input ~ 0
~DSCHG~
Text Label 650  4850 0    60   ~ 0
CFOA_DET_IN
Text Label 2800 4850 0    60   ~ 0
CFOA_DET_OUT
Text Label 2350 6350 0    60   Italic 0
CFOA_DET_IN
Text Label 2250 6350 2    60   Italic 0
CFOA_DET_OUT
Text HLabel 1150 6200 0    60   Input ~ 0
RF_OUT
$Comp
L R RB1
U 1 1 59001DCB
P 1350 6050
F 0 "RB1" V 1430 6050 50  0000 C CNN
F 1 "R" V 1350 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1280 6050 50  0001 C CNN
F 3 "" H 1350 6050 50  0000 C CNN
	1    1350 6050
	0    -1   -1   0   
$EndComp
$Comp
L R RB2
U 1 1 59004598
P 1350 6350
F 0 "RB2" V 1430 6350 50  0000 C CNN
F 1 "R" V 1350 6350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 1280 6350 50  0001 C CNN
F 3 "" H 1350 6350 50  0000 C CNN
	1    1350 6350
	0    -1   -1   0   
$EndComp
Text Notes 1450 4150 0    60   ~ 0
PEAK DETECTOR 2
$Comp
L VCC #PWR29
U 1 1 59004736
P 5900 6850
F 0 "#PWR29" H 5900 6700 50  0001 C CNN
F 1 "VCC" H 5900 7000 50  0000 C CNN
F 2 "" H 5900 6850 50  0000 C CNN
F 3 "" H 5900 6850 50  0000 C CNN
	1    5900 6850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR28
U 1 1 590047A8
P 5700 7200
F 0 "#PWR28" H 5700 6950 50  0001 C CNN
F 1 "GND" H 5700 7050 50  0000 C CNN
F 2 "" H 5700 7200 50  0000 C CNN
F 3 "" H 5700 7200 50  0000 C CNN
	1    5700 7200
	1    0    0    -1  
$EndComp
$Comp
L VEE #PWR30
U 1 1 5900481A
P 5900 7500
F 0 "#PWR30" H 5900 7350 50  0001 C CNN
F 1 "VEE" H 5900 7650 50  0000 C CNN
F 2 "" H 5900 7500 50  0000 C CNN
F 3 "" H 5900 7500 50  0000 C CNN
	1    5900 7500
	-1   0    0    1   
$EndComp
$Comp
L CP1 CR5
U 1 1 59004EC1
P 5900 7000
F 0 "CR5" H 5925 7100 50  0000 L CNN
F 1 "22u" H 5925 6900 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 5900 7000 50  0001 C CNN
F 3 "" H 5900 7000 50  0000 C CNN
	1    5900 7000
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR7
U 1 1 59006A1C
P 6150 7000
F 0 "CR7" H 6175 7100 50  0000 L CNN
F 1 "22u" H 6175 6900 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 6150 7000 50  0001 C CNN
F 3 "" H 6150 7000 50  0000 C CNN
	1    6150 7000
	1    0    0    -1  
$EndComp
Text Notes 5350 6550 0    60   ~ 0
Reservoir caps\n4 for amplifier blocks\n1 for LTC5507\n1 for the LTC output buffer\nPeak Detector 2 has its reservoirs in its own sheet
$Comp
L AD8011 U6
U 1 1 590114EC
P 9750 3000
F 0 "U6" H 9800 2800 50  0000 L CNN
F 1 "AD8011" H 9800 2900 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 9800 3050 50  0001 C CNN
F 3 "" H 9900 3150 50  0000 C CNN
	1    9750 3000
	1    0    0    -1  
$EndComp
$Comp
L R RF2
U 1 1 59012A39
P 9750 2550
F 0 "RF2" V 9650 2550 50  0000 C CNN
F 1 "R" V 9750 2550 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9680 2550 50  0001 C CNN
F 3 "" H 9750 2550 50  0000 C CNN
	1    9750 2550
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR43
U 1 1 59013B82
P 9650 2700
F 0 "#PWR43" H 9650 2550 50  0001 C CNN
F 1 "VCC" H 9650 2850 50  0000 C CNN
F 2 "" H 9650 2700 50  0000 C CNN
F 3 "" H 9650 2700 50  0000 C CNN
	1    9650 2700
	0    1    1    0   
$EndComp
$Comp
L VEE #PWR44
U 1 1 59013C00
P 9650 3300
F 0 "#PWR44" H 9650 3150 50  0001 C CNN
F 1 "VEE" H 9650 3450 50  0000 C CNN
F 2 "" H 9650 3300 50  0000 C CNN
F 3 "" H 9650 3300 50  0000 C CNN
	1    9650 3300
	0    -1   -1   0   
$EndComp
$Comp
L R RG2
U 1 1 59015001
P 9300 2900
F 0 "RG2" V 9380 2900 50  0000 C CNN
F 1 "R" V 9300 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9230 2900 50  0001 C CNN
F 3 "" H 9300 2900 50  0000 C CNN
	1    9300 2900
	0    -1   -1   0   
$EndComp
$Comp
L R RO2
U 1 1 59016C41
P 10250 3000
F 0 "RO2" V 10330 3000 50  0000 C CNN
F 1 "R" V 10250 3000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 10180 3000 50  0001 C CNN
F 3 "" H 10250 3000 50  0000 C CNN
	1    10250 3000
	0    1    1    0   
$EndComp
Text Notes 9150 2750 0    15   ~ 0
RG to (-) path must be short!
$Comp
L R RS2
U 1 1 5901C183
P 9050 3100
F 0 "RS2" V 9130 3100 50  0000 C CNN
F 1 "R" V 9050 3100 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8980 3100 50  0001 C CNN
F 3 "" H 9050 3100 50  0000 C CNN
	1    9050 3100
	0    1    1    0   
$EndComp
$Comp
L R RS3
U 1 1 5901D1C5
P 9250 3300
F 0 "RS3" V 9330 3300 50  0000 C CNN
F 1 "R" V 9250 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9180 3300 50  0001 C CNN
F 3 "" H 9250 3300 50  0000 C CNN
	1    9250 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR42
U 1 1 5901D5E1
P 9250 3450
F 0 "#PWR42" H 9250 3200 50  0001 C CNN
F 1 "GND" H 9250 3300 50  0000 C CNN
F 2 "" H 9250 3450 50  0000 C CNN
F 3 "" H 9250 3450 50  0000 C CNN
	1    9250 3450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR41
U 1 1 5901DC18
P 9150 2950
F 0 "#PWR41" H 9150 2700 50  0001 C CNN
F 1 "GND" H 9050 2950 50  0000 C CNN
F 2 "" H 9150 2950 50  0000 C CNN
F 3 "" H 9150 2950 50  0000 C CNN
	1    9150 2950
	1    0    0    -1  
$EndComp
Text Notes 9450 2400 0    60   ~ 0
Output Driver
$Comp
L C C1
U 1 1 5902A193
P 1400 3100
F 0 "C1" H 1425 3200 50  0000 L CNN
F 1 "19 pF" H 1425 3000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1438 2950 50  0001 C CNN
F 3 "" H 1400 3100 50  0000 C CNN
	1    1400 3100
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5902A199
P 2100 3100
F 0 "C3" H 2125 3200 50  0000 L CNN
F 1 "19 pF" H 2125 3000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2138 2950 50  0001 C CNN
F 3 "" H 2100 3100 50  0000 C CNN
	1    2100 3100
	1    0    0    -1  
$EndComp
$Comp
L L L1
U 1 1 5902A19F
P 1750 2900
F 0 "L1" V 1700 2900 50  0000 C CNN
F 1 "52.84 nH" V 1825 2900 50  0000 C CNN
F 2 "footprints:L_0603_HandSoldering" H 1750 2900 50  0001 C CNN
F 3 "" H 1750 2900 50  0000 C CNN
	1    1750 2900
	0    1    1    0   
$EndComp
$Comp
L GND #PWR13
U 1 1 5902A1A5
P 1400 3250
F 0 "#PWR13" H 1400 3000 50  0001 C CNN
F 1 "GND" H 1400 3100 50  0000 C CNN
F 2 "" H 1400 3250 50  0000 C CNN
F 3 "" H 1400 3250 50  0000 C CNN
	1    1400 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR15
U 1 1 5902A1AB
P 2100 3250
F 0 "#PWR15" H 2100 3000 50  0001 C CNN
F 1 "GND" H 2100 3100 50  0000 C CNN
F 2 "" H 2100 3250 50  0000 C CNN
F 3 "" H 2100 3250 50  0000 C CNN
	1    2100 3250
	1    0    0    -1  
$EndComp
Text Notes 1400 2600 0    60   ~ 0
Acquisition Filter 1
Text Notes 500  3700 0    60   ~ 0
 (s-parameters @ qucs "bpromfe_acquisition_filter.sch")
Text Notes 650  3350 0    60   Italic 0
240 MHz \n0.1 dB ripple \nLPF Chebyshev
Text Label 700  2900 0    60   ~ 0
ACQ_FILT1_IN
Text Label 2900 2900 2    60   ~ 0
ACQ_FILT1_OUT
Text Notes 2300 2450 0    60   ~ 0
General purpose 3rd order filters.\nCan be adapted for different freqs.
Text Label 5950 1250 2    60   ~ 0
ACQ_FILT1_IN
Text Label 5700 1350 0    60   ~ 0
ACQ_FILT1_OUT
$Comp
L C CBH5
U 1 1 59064B0B
P 1150 7300
F 0 "CBH5" H 1175 7400 50  0000 L CNN
F 1 "0.1uF" H 1200 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1188 7150 50  0001 C CNN
F 3 "" H 1150 7300 50  0000 C CNN
	1    1150 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBL5
U 1 1 59064B11
P 1450 7300
F 0 "CBL5" H 1475 7400 50  0000 L CNN
F 1 "100pF" H 1475 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1488 7150 50  0001 C CNN
F 3 "" H 1450 7300 50  0000 C CNN
	1    1450 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBH6
U 1 1 59065261
P 1750 7300
F 0 "CBH6" H 1775 7400 50  0000 L CNN
F 1 "0.1uF" H 1800 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1788 7150 50  0001 C CNN
F 3 "" H 1750 7300 50  0000 C CNN
	1    1750 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBL6
U 1 1 59065267
P 2050 7300
F 0 "CBL6" H 2075 7400 50  0000 L CNN
F 1 "100pF" H 2075 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2088 7150 50  0001 C CNN
F 3 "" H 2050 7300 50  0000 C CNN
	1    2050 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBH7
U 1 1 590658EE
P 2350 7300
F 0 "CBH7" H 2375 7400 50  0000 L CNN
F 1 "0.1uF" H 2400 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2388 7150 50  0001 C CNN
F 3 "" H 2350 7300 50  0000 C CNN
	1    2350 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBL7
U 1 1 590658F4
P 2650 7300
F 0 "CBL7" H 2675 7400 50  0000 L CNN
F 1 "100pF" H 2675 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2688 7150 50  0001 C CNN
F 3 "" H 2650 7300 50  0000 C CNN
	1    2650 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBH8
U 1 1 590658FC
P 2950 7300
F 0 "CBH8" H 2975 7400 50  0000 L CNN
F 1 "0.1uF" H 3000 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2988 7150 50  0001 C CNN
F 3 "" H 2950 7300 50  0000 C CNN
	1    2950 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBL8
U 1 1 59065902
P 3250 7300
F 0 "CBL8" H 3275 7400 50  0000 L CNN
F 1 "100pF" H 3275 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3288 7150 50  0001 C CNN
F 3 "" H 3250 7300 50  0000 C CNN
	1    3250 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBH9
U 1 1 59065B50
P 3550 7300
F 0 "CBH9" H 3575 7400 50  0000 L CNN
F 1 "0.1uF" H 3600 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3588 7150 50  0001 C CNN
F 3 "" H 3550 7300 50  0000 C CNN
	1    3550 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBL9
U 1 1 59065B56
P 3850 7300
F 0 "CBL9" H 3875 7400 50  0000 L CNN
F 1 "100pF" H 3875 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3888 7150 50  0001 C CNN
F 3 "" H 3850 7300 50  0000 C CNN
	1    3850 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBH10
U 1 1 59065B5E
P 4150 7300
F 0 "CBH10" H 4175 7400 50  0000 L CNN
F 1 "0.1uF" H 4200 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4188 7150 50  0001 C CNN
F 3 "" H 4150 7300 50  0000 C CNN
	1    4150 7300
	1    0    0    -1  
$EndComp
$Comp
L C CBL10
U 1 1 59065B64
P 4450 7300
F 0 "CBL10" H 4475 7400 50  0000 L CNN
F 1 "100pF" H 4475 7200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4488 7150 50  0001 C CNN
F 3 "" H 4450 7300 50  0000 C CNN
	1    4450 7300
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR9
U 1 1 5906A014
P 6400 7000
F 0 "CR9" H 6425 7100 50  0000 L CNN
F 1 "22u" H 6425 6900 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 6400 7000 50  0001 C CNN
F 3 "" H 6400 7000 50  0000 C CNN
	1    6400 7000
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR10
U 1 1 5906A01A
P 6400 7300
F 0 "CR10" H 6425 7400 50  0000 L CNN
F 1 "22u" H 6425 7200 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 6400 7300 50  0001 C CNN
F 3 "" H 6400 7300 50  0000 C CNN
	1    6400 7300
	1    0    0    -1  
$EndComp
Text Notes 500  7750 0    60   ~ 0
1 pair for each of the 4 amps at the input\n1 pair for LTC5507\n1 pair for LTC5507 output buffer
$Comp
L Jumper_NO_Small JP1
U 1 1 5902E60C
P 8650 3400
F 0 "JP1" H 8650 3480 50  0000 C CNN
F 1 "~SHDN~" H 8660 3340 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8650 3400 50  0001 C CNN
F 3 "" H 8650 3400 50  0000 C CNN
	1    8650 3400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR40
U 1 1 5902FCBA
P 8800 3450
F 0 "#PWR40" H 8800 3200 50  0001 C CNN
F 1 "GND" H 8800 3300 50  0000 C CNN
F 2 "" H 8800 3450 50  0000 C CNN
F 3 "" H 8800 3450 50  0000 C CNN
	1    8800 3450
	1    0    0    -1  
$EndComp
Text Notes 3050 5900 0    60   ~ 0
Select route with RB
$Comp
L R RA1
U 1 1 5901DFAE
P 7800 1050
F 0 "RA1" V 7880 1050 50  0000 C CNN
F 1 "R" V 7800 1050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7730 1050 50  0001 C CNN
F 3 "" H 7800 1050 50  0000 C CNN
	1    7800 1050
	1    0    0    1   
$EndComp
$Comp
L R RA2
U 1 1 5901E08D
P 8000 1250
F 0 "RA2" V 8080 1250 50  0000 C CNN
F 1 "R" V 8000 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7930 1250 50  0001 C CNN
F 3 "" H 8000 1250 50  0000 C CNN
	1    8000 1250
	0    1    -1   0   
$EndComp
$Comp
L R RA3
U 1 1 5901E14C
P 8200 1050
F 0 "RA3" V 8280 1050 50  0000 C CNN
F 1 "R" V 8200 1050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8130 1050 50  0001 C CNN
F 3 "" H 8200 1050 50  0000 C CNN
	1    8200 1050
	1    0    0    1   
$EndComp
$Comp
L GND #PWR35
U 1 1 59020B2B
P 8000 900
F 0 "#PWR35" H 8000 650 50  0001 C CNN
F 1 "GND" H 8000 750 50  0000 C CNN
F 2 "" H 8000 900 50  0000 C CNN
F 3 "" H 8000 900 50  0000 C CNN
	1    8000 900 
	1    0    0    -1  
$EndComp
Text Notes 7700 750  0    30   ~ 0
Simple Matched Attenuator
Text Notes 3950 5000 0    60   ~ 0
CAPACITOR REF NAMING\n=================\nCBL: Lo Val Byp. Cap CBM: Medium CBH: High\nCBR: Reservoir cap\nCLP: LTC5507 Hold Cap\nCD: DC Block Cap
$Comp
L R R6
U 1 1 590648CD
P 3350 4300
F 0 "R6" V 3430 4300 50  0000 C CNN
F 1 "22k" V 3350 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3280 4300 50  0001 C CNN
F 3 "" H 3350 4300 50  0000 C CNN
	1    3350 4300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR17
U 1 1 590648D4
P 3350 4100
F 0 "#PWR17" H 3350 3950 50  0001 C CNN
F 1 "VCC" H 3350 4250 50  0000 C CNN
F 2 "" H 3350 4100 50  0000 C CNN
F 3 "" H 3350 4100 50  0000 C CNN
	1    3350 4100
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NO_Small JP2
U 1 1 590648DE
P 3500 4500
F 0 "JP2" H 3500 4580 50  0000 C CNN
F 1 "~SHDN~" H 3510 4440 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3500 4500 50  0001 C CNN
F 3 "" H 3500 4500 50  0000 C CNN
	1    3500 4500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR19
U 1 1 590648E6
P 3650 4550
F 0 "#PWR19" H 3650 4300 50  0001 C CNN
F 1 "GND" H 3650 4400 50  0000 C CNN
F 2 "" H 3650 4550 50  0000 C CNN
F 3 "" H 3650 4550 50  0000 C CNN
	1    3650 4550
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR15
U 1 1 5906CDE8
P 5500 7000
F 0 "CR15" H 5525 7100 50  0000 L CNN
F 1 "22u" H 5525 6900 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 5500 7000 50  0001 C CNN
F 3 "" H 5500 7000 50  0000 C CNN
	1    5500 7000
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR13
U 1 1 5906D0A0
P 5300 7000
F 0 "CR13" H 5325 7100 50  0000 L CNN
F 1 "22u" H 5325 6900 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 5300 7000 50  0001 C CNN
F 3 "" H 5300 7000 50  0000 C CNN
	1    5300 7000
	1    0    0    -1  
$EndComp
$Comp
L CP1 CR11
U 1 1 5906D368
P 5100 7000
F 0 "CR11" H 5125 7100 50  0000 L CNN
F 1 "22u" H 5125 6900 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 5100 7000 50  0001 C CNN
F 3 "" H 5100 7000 50  0000 C CNN
	1    5100 7000
	1    0    0    -1  
$EndComp
$Comp
L VEE #PWR11
U 1 1 590728CA
P 700 7250
F 0 "#PWR11" H 700 7100 50  0001 C CNN
F 1 "VEE" H 700 7400 50  0000 C CNN
F 2 "" H 700 7250 50  0000 C CNN
F 3 "" H 700 7250 50  0000 C CNN
	1    700  7250
	-1   0    0    1   
$EndComp
$Comp
L C CBH15
U 1 1 590729B3
P 600 7100
F 0 "CBH15" H 625 7200 50  0000 L CNN
F 1 "0.1uF" H 650 7000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 638 6950 50  0001 C CNN
F 3 "" H 600 7100 50  0000 C CNN
	1    600  7100
	1    0    0    -1  
$EndComp
$Comp
L C CBL15
U 1 1 59072AC2
P 900 7100
F 0 "CBL15" H 925 7200 50  0000 L CNN
F 1 "100pF" H 925 7000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 938 6950 50  0001 C CNN
F 3 "" H 900 7100 50  0000 C CNN
	1    900  7100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR12
U 1 1 59072F41
P 1200 6950
F 0 "#PWR12" H 1200 6700 50  0001 C CNN
F 1 "GND" H 1200 6800 50  0000 C CNN
F 2 "" H 1200 6950 50  0000 C CNN
F 3 "" H 1200 6950 50  0000 C CNN
	1    1200 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 1550 10200 1550
Wire Wire Line
	7350 1550 7250 1550
Wire Wire Line
	10500 1250 11100 1250
Wire Notes Line
	3900 650  11150 650 
Wire Wire Line
	1700 1250 2100 1250
Wire Wire Line
	1900 1300 1900 1250
Connection ~ 1900 1250
Wire Wire Line
	2400 1250 3550 1250
Wire Wire Line
	2600 1250 2600 1300
Wire Wire Line
	1400 1250 700  1250
Connection ~ 2600 1250
Wire Notes Line
	600  1000 600  1900
Wire Notes Line
	600  1900 3600 1900
Wire Notes Line
	3600 1900 3600 1000
Wire Notes Line
	3600 1000 600  1000
Wire Wire Line
	7800 1850 7800 1650
Wire Notes Line
	11150 650  11150 2050
Wire Notes Line
	11150 2050 3900 2050
Wire Notes Line
	3900 2050 3900 650 
Wire Wire Line
	6600 1850 7800 1850
Wire Wire Line
	6600 1850 6600 1600
Wire Wire Line
	6400 1250 6700 1250
Wire Wire Line
	7000 1250 7050 1250
Wire Wire Line
	6600 1300 6600 1250
Connection ~ 6600 1250
Wire Wire Line
	9850 1250 9900 1250
Wire Wire Line
	7050 1850 7050 1850
Connection ~ 7050 1850
Wire Wire Line
	3300 2900 4200 2900
Wire Wire Line
	4000 2950 4000 2900
Connection ~ 4000 2900
Wire Wire Line
	4500 2900 5500 2900
Wire Wire Line
	4700 2900 4700 2950
Connection ~ 4700 2900
Wire Notes Line
	3200 2650 3200 3550
Wire Notes Line
	5650 3550 5650 2650
Wire Wire Line
	7200 2850 7200 2950
Wire Wire Line
	6400 3050 7500 3050
Wire Wire Line
	8400 2950 8600 2950
Wire Wire Line
	8600 2950 8600 3000
Wire Wire Line
	8400 3050 8450 3050
Wire Wire Line
	7200 2950 7500 2950
Wire Wire Line
	7450 2850 7500 2850
Wire Wire Line
	7250 2850 7200 2850
Wire Wire Line
	7500 2850 7500 2650
Wire Notes Line
	6300 2250 10550 2250
Wire Notes Line
	10550 2250 10550 4300
Wire Notes Line
	10550 4300 6300 4300
Wire Notes Line
	6300 4300 6300 2250
Wire Wire Line
	7450 3100 7450 3050
Connection ~ 7450 3050
Wire Wire Line
	4650 1250 4000 1250
Wire Wire Line
	1150 5750 1800 5750
Wire Wire Line
	1900 5750 4100 5750
Wire Wire Line
	4200 5750 5000 5750
Wire Wire Line
	5000 5750 5000 6200
Wire Wire Line
	5000 6200 4300 6200
Wire Wire Line
	1500 6050 2250 6050
Wire Wire Line
	4700 7450 4700 7500
Wire Wire Line
	4700 7150 4700 7100
Wire Notes Line
	5650 2650 3200 2650
Wire Notes Line
	3200 3550 5650 3550
Wire Notes Line
	4950 7750 4950 6900
Wire Notes Line
	500  7750 4950 7750
Wire Notes Line
	500  7750 500  6850
Wire Notes Line
	500  6850 4950 6850
Wire Notes Line
	4950 6850 4950 6950
Wire Wire Line
	8900 1550 8800 1550
Wire Wire Line
	9400 1850 9400 1650
Wire Wire Line
	8200 1850 9400 1850
Wire Wire Line
	8200 1850 8200 1650
Wire Wire Line
	8550 1250 8600 1250
Wire Wire Line
	9200 1250 9550 1250
Wire Wire Line
	9400 1250 9400 1350
Connection ~ 9400 1250
Wire Wire Line
	8600 1850 8600 1850
Connection ~ 8600 1850
Wire Wire Line
	4850 900  4850 950 
Wire Wire Line
	4950 900  4850 900 
Wire Wire Line
	5250 900  5250 1000
Wire Wire Line
	2800 4850 2750 4850
Wire Wire Line
	1250 4850 650  4850
Wire Wire Line
	1500 6350 2250 6350
Wire Wire Line
	1200 6050 1200 6350
Wire Wire Line
	1200 6200 1150 6200
Connection ~ 1200 6200
Wire Wire Line
	2350 6050 2850 6050
Wire Wire Line
	2900 6350 2350 6350
Wire Wire Line
	3150 6050 3200 6050
Wire Wire Line
	3200 6050 3200 6350
Connection ~ 3200 6200
Wire Notes Line
	5100 5600 5100 6500
Wire Notes Line
	5100 6500 650  6500
Wire Notes Line
	650  6500 650  5600
Wire Notes Line
	650  5600 5100 5600
Wire Notes Line
	3050 4200 3050 5200
Wire Notes Line
	3050 5200 600  5200
Wire Notes Line
	600  5200 600  4200
Wire Notes Line
	600  4200 3050 4200
Wire Wire Line
	8400 3400 8550 3400
Wire Wire Line
	5700 7150 5700 7200
Wire Wire Line
	5100 7150 6400 7150
Connection ~ 5900 7150
Wire Wire Line
	5100 6850 6400 6850
Connection ~ 6150 7150
Wire Notes Line
	5050 6650 5050 7700
Wire Notes Line
	5050 7700 6750 7700
Wire Notes Line
	6750 7700 6750 6650
Wire Notes Line
	6750 6650 5050 6650
Wire Wire Line
	10050 3000 10050 2550
Wire Wire Line
	10050 2550 9900 2550
Wire Wire Line
	9600 2550 9450 2550
Wire Wire Line
	9450 2550 9450 2900
Wire Wire Line
	10100 3000 10050 3000
Wire Wire Line
	9200 3100 9450 3100
Wire Wire Line
	9250 3150 9250 3100
Connection ~ 9250 3100
Wire Wire Line
	9150 2900 9150 2950
Wire Wire Line
	8900 3100 8900 2850
Wire Wire Line
	8900 2850 8400 2850
Wire Notes Line
	8850 2400 8850 3600
Wire Notes Line
	8850 3600 10450 3600
Wire Notes Line
	10450 3600 10450 2400
Wire Notes Line
	10450 2400 8850 2400
Wire Wire Line
	10400 3000 10950 3000
Wire Wire Line
	700  2900 1600 2900
Wire Wire Line
	1400 2950 1400 2900
Connection ~ 1400 2900
Wire Wire Line
	1900 2900 2900 2900
Wire Wire Line
	2100 2900 2100 2950
Connection ~ 2100 2900
Wire Notes Line
	600  2650 600  3550
Wire Notes Line
	3050 3550 3050 2650
Wire Notes Line
	3050 2650 600  2650
Wire Notes Line
	600  3550 3050 3550
Wire Wire Line
	5250 1250 5950 1250
Wire Wire Line
	5700 1350 6400 1350
Wire Wire Line
	6400 1350 6400 1250
Wire Wire Line
	8450 3050 8450 3400
Wire Wire Line
	1150 7150 4700 7150
Wire Wire Line
	1150 7450 4700 7450
Connection ~ 1450 7450
Connection ~ 1450 7150
Connection ~ 2050 7450
Connection ~ 2050 7150
Connection ~ 1750 7150
Connection ~ 1750 7450
Connection ~ 2650 7450
Connection ~ 2650 7150
Connection ~ 3250 7450
Connection ~ 3250 7150
Connection ~ 2950 7150
Connection ~ 2950 7450
Connection ~ 3850 7450
Connection ~ 3850 7150
Connection ~ 4450 7450
Connection ~ 4450 7150
Connection ~ 4150 7150
Connection ~ 4150 7450
Connection ~ 3550 7150
Connection ~ 3550 7450
Connection ~ 2350 7150
Connection ~ 2350 7450
Connection ~ 6400 7150
Connection ~ 6150 6850
Connection ~ 8450 3400
Wire Wire Line
	8800 3450 8800 3400
Wire Wire Line
	8800 3400 8750 3400
Wire Wire Line
	8200 1200 8200 1350
Wire Wire Line
	8150 1250 8250 1250
Wire Wire Line
	7650 1250 7850 1250
Wire Wire Line
	7800 1200 7800 1350
Connection ~ 7800 1250
Connection ~ 8200 1250
Wire Wire Line
	7800 900  7800 850 
Wire Wire Line
	7800 850  8200 850 
Wire Wire Line
	8000 850  8000 900 
Wire Wire Line
	8200 850  8200 900 
Connection ~ 8000 850 
Wire Notes Line
	7700 750  7700 1300
Wire Notes Line
	7700 1300 8300 1300
Wire Notes Line
	8300 1300 8300 750 
Wire Notes Line
	8300 750  7700 750 
Wire Wire Line
	3350 4100 3350 4150
Wire Wire Line
	2900 4500 3400 4500
Wire Wire Line
	3350 4500 3350 4450
Connection ~ 3350 4500
Wire Wire Line
	3650 4550 3650 4500
Wire Wire Line
	3650 4500 3600 4500
Wire Wire Line
	2900 4500 2900 4650
Wire Wire Line
	2900 4650 2750 4650
Connection ~ 5900 6850
Connection ~ 5700 7150
Connection ~ 5500 6850
Connection ~ 5300 6850
Connection ~ 5300 7150
Connection ~ 5500 7150
Wire Wire Line
	600  7250 900  7250
Connection ~ 700  7250
Wire Wire Line
	600  6950 1200 6950
Connection ~ 900  6950
Wire Wire Line
	5900 7450 6400 7450
Wire Wire Line
	5900 7450 5900 7500
$Comp
L R RB7
U 1 1 5906AFD0
P 3000 6050
F 0 "RB7" V 3080 6050 50  0000 C CNN
F 1 "R" V 3000 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2930 6050 50  0001 C CNN
F 3 "" H 3000 6050 50  0000 C CNN
	1    3000 6050
	0    1    1    0   
$EndComp
$Comp
L R RB8
U 1 1 5906B1BB
P 3050 6350
F 0 "RB8" V 3130 6350 50  0000 C CNN
F 1 "R" V 3050 6350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2980 6350 50  0001 C CNN
F 3 "" H 3050 6350 50  0000 C CNN
	1    3050 6350
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 6200 3200 6200
$Comp
L TEST_2P W1
U 1 1 5906CBD0
P 3600 6450
F 0 "W1" H 3600 6510 50  0000 C CNN
F 1 "TEST_2P" H 3750 6300 50  0000 C CNN
F 2 "footprints:SMD-COAX-SOLDER" H 3600 6450 50  0001 C CNN
F 3 "" H 3600 6450 50  0000 C CNN
	1    3600 6450
	0    1    1    0   
$EndComp
$Comp
L GND #PWR18
U 1 1 5906DC50
P 3600 6650
F 0 "#PWR18" H 3600 6400 50  0001 C CNN
F 1 "GND" H 3600 6500 50  0000 C CNN
F 2 "" H 3600 6650 50  0000 C CNN
F 3 "" H 3600 6650 50  0000 C CNN
	1    3600 6650
	1    0    0    -1  
$EndComp
Text Label 4200 6100 2    60   ~ 0
ACQ_FILT2_OUT
$Comp
L R RB9
U 1 1 5906E727
P 3800 6200
F 0 "RB9" V 3880 6200 50  0000 C CNN
F 1 "R" V 3800 6200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3730 6200 50  0001 C CNN
F 3 "" H 3800 6200 50  0000 C CNN
	1    3800 6200
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 6100 4200 6200
Wire Wire Line
	4200 6200 3950 6200
Wire Wire Line
	3550 6200 3650 6200
Wire Wire Line
	3600 6250 3600 6200
Connection ~ 3600 6200
Wire Wire Line
	3600 6650 3600 6650
$Comp
L RF_Shield_One_Piece J1
U 1 1 59074D29
P 4500 1750
F 0 "J1" H 4500 1950 50  0000 C CNN
F 1 "LNA_Shield" H 4350 1800 50  0000 C CNN
F 2 "Shielding_Cabinets:Würth_36103255_25x25mm" H 4500 1650 50  0001 C CNN
F 3 "" H 4500 1650 50  0000 C CNN
	1    4500 1750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR21
U 1 1 59076FA4
P 4500 2200
F 0 "#PWR21" H 4500 1950 50  0001 C CNN
F 1 "GND" H 4500 2050 50  0000 C CNN
F 2 "" H 4500 2200 50  0000 C CNN
F 3 "" H 4500 2200 50  0000 C CNN
	1    4500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2150 4500 2200
$EndSCHEMATC
