# ERRATA
## BPROMFE Demo Board Rev A
 - The pin 7 of U6 is not connected to the via in front of it. A short piece of wire
   must be soldered to bridge the connections for the correct operation of
   LTC5507 peak detector. Corrected this error in the development files.
