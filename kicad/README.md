# Folder Structure

 1. footprints contain kicad footprints
 2. libs contain kicad symbol libraries
 3. example is an example layout for AD8000 buffer amplifier configuration.

# Note
There can be another efficient way of dealing with KiCad libraries such as using
the github library repositories directly. Since the 3rd party stuff we require
is small, I didn't adopt such stuff.
